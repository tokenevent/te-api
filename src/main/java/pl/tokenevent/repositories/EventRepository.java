package pl.tokenevent.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.tokenevent.domain.Event;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends PagingAndSortingRepository<Event, Long> {
    Optional<Event> findById(String id);

    List<Event> findEventsByCreatorId(String creatorId);
}
