package pl.tokenevent.repositories

import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import pl.tokenevent.domain.BuyTokenTransaction

@Repository
interface BuyTokenTransactionsRepository : PagingAndSortingRepository<BuyTokenTransaction, String> {
    override fun findAll(): List<BuyTokenTransaction>
    fun findAllByUserId(userId: String, pageable: Pageable): List<BuyTokenTransaction>
    fun findAllByEventIdOrderByCreatedAtDesc(eventId: String, pageable: Pageable): List<BuyTokenTransaction>
}
