package pl.tokenevent.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import pl.tokenevent.domain.Promotion

@Repository
interface PromoCodeRepository : MongoRepository<Promotion, String> {
    fun findAllByCode(code: String): Promotion?
}
