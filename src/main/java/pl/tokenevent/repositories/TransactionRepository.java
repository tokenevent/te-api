package pl.tokenevent.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.tokenevent.domain.Transaction;
import pl.tokenevent.domain.TransactionStatus;

import java.util.List;

@Repository
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, String> {
    List<Transaction> findAll();

    List<Transaction> findAllByScannerUserId(String scannerUserId);

    List<Transaction> findAllByScannerUserId(String scannerUserId, Pageable pageable);

    List<Transaction> findAllByUserIdAndStatus(String userId, TransactionStatus status, Pageable pageable);

    List<Transaction> findAllByUserId(String userId);

    List<Transaction> findAllByUserId(String userId, Pageable pageable);

    List<Transaction> findAllByEventId(String eventId);

    List<Transaction> findAllByEventIdOrderByTransactionAtDesc(String eventId, Pageable pageable);

    List<Transaction> findAllByStatus(TransactionStatus status, Pageable pageable);

    List<Transaction> findAllByStatus(TransactionStatus status);
}
