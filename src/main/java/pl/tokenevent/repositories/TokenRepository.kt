package pl.tokenevent.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import pl.tokenevent.domain.Token

@Repository
interface TokenRepository : MongoRepository<Token, String> {
    fun findAllByOwnerId(ownerId: String): List<Token>
    fun findFirstByBuyableTokenName(buyableTokenName: String): Token?
}
