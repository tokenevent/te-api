package pl.tokenevent.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.tokenevent.domain.Image;

public interface ImagesRepository extends PagingAndSortingRepository<Image, String> {
}
