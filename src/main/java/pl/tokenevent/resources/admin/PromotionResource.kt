package pl.tokenevent.resources.admin

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.domain.Promotion
import pl.tokenevent.infra.getUserIdFrom
import pl.tokenevent.infra.promocode.PromoCodeService
import java.lang.invoke.MethodHandles
import java.security.Principal

@CrossOrigin(origins = ["http://localhost:3000", "http://te-web-client.herokuapp.com"])
@RestController
class PromotionResource(private val promoCodeService: PromoCodeService) {

    @PostMapping("/admin/promo")
    fun addPromotion(
        @RequestBody promoRequest: PromoRequest,
        principal: Principal
    ): ResponseEntity<Promotion> {
        val creatorId = getUserIdFrom(principal)

        logger.info("{} wants to add {} promo code", creatorId, promoRequest.code)
        return ResponseEntity(promoCodeService.addPromoCode(promoRequest.code, promoRequest.eventId), HttpStatus.CREATED)
    }

    data class PromoRequest(
        val code: String,
        val eventId: String
    )

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }
}
