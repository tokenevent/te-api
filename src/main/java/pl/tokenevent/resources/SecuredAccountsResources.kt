package pl.tokenevent.resources

import org.springframework.http.ResponseEntity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.exceptions.UsernameTakenException
import pl.tokenevent.infra.Roles
import pl.tokenevent.infra.Roles.ROLE_USER
import pl.tokenevent.infra.users.User
import pl.tokenevent.infra.users.UserService
import pl.tokenevent.resources.dto.LoginFormDTO
import pl.tokenevent.resources.dto.UserResponse
import java.security.Principal

@RestController
class SecuredAccountsResources(
    val userService: UserService
) {
    @PostMapping("/admin/register/events-manager")
    fun registerEventManager(@RequestBody loginFormDTO: LoginFormDTO): ResponseEntity<UserResponse> {
        if (userService.findByUsername(loginFormDTO.username).isPresent) {
            throw UsernameTakenException()
        }
        val authorities = mutableListOf<GrantedAuthority>()
        val roleUser = ROLE_USER
        val eventManager = Roles.ROLE_EVENT_MANAGER
        authorities.add(SimpleGrantedAuthority(roleUser.toString()))
        authorities.add(SimpleGrantedAuthority(eventManager.toString()))

        return ResponseEntity.ok(UserResponse(userService.saveUser(User.adminOf(loginFormDTO, authorities))))
    }

    @GetMapping("/admin/echo")
    fun getAdmin(
        principal: Principal
    ): Test {
        return Test("this is available for admin")
    }

    @GetMapping("/scanner/echo")
    fun getScanner(
        principal: Principal
    ): Test {
        return Test("this is available for scanner")
    }

    @GetMapping("/echo")
    @ResponseBody
    operator fun get(
        principal: Principal
    ): Test {
        return Test("this is available for all")
    }


    data class Test(val test: String)
}
