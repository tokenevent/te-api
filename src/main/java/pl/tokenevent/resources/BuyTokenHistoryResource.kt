package pl.tokenevent.resources

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.domain.BuyTokenTransaction
import pl.tokenevent.domain.BuyTokenTransactionStatus
import pl.tokenevent.domain.GroupedTokenDetails
import pl.tokenevent.infra.buytokenhistory.BuyTokenHistoryService
import pl.tokenevent.infra.buytokenhistory.SingleTransactionUpdateStatus
import pl.tokenevent.infra.getUserIdFrom
import java.security.Principal
import java.time.Instant

@RestController
class BuyTokenHistoryResource(
    val buyTokenHistoryService: BuyTokenHistoryService
) {
    /*
        endpoint for user to get his buy-token history
    */
    @GetMapping("/my/buy-token-history")
    fun getMyBuyTokenHistory(
        principal: Principal,
        @RequestParam page: Int?
    ): ResponseEntity<BuyTokenHistoryList> {
        val userId = getUserIdFrom(principal)
        val transactions = buyTokenHistoryService.findAllByUserId(userId, page).sortedBy { it.createdAt }.reversed()
        return ResponseEntity.ok(
            BuyTokenHistoryList(
                transactions.map { BuyTokenTransactionResponse.of(it) }
            ))
    }

    /*
        endpoint for user's apps. Should be used as polling in order to show notification from payu.
     */
    @GetMapping("/my/buy-token-history/{id}")
    fun getTransactionStatusById(principal: Principal, @PathVariable id: String): ResponseEntity<SingleTransactionUpdateStatus> {
        val userId = getUserIdFrom(principal)
        val singleTransactionStatus = buyTokenHistoryService.findSingleTransactionUpdateStatus(id, userId)

        return ResponseEntity.ok(singleTransactionStatus)
    }

    @GetMapping("/admin/buy-token-history")
    fun getAllTransactions(principal: Principal): ResponseEntity<BuyTokenHistoryList> {
        val transaction = buyTokenHistoryService.findAll()
        return ResponseEntity.ok(BuyTokenHistoryList(transaction.map { BuyTokenTransactionResponse.of(it) }))
    }

    @GetMapping("/admin/buy-token-history/{userId}")
    fun getUsersTransactions(
        principal: Principal,
        @PathVariable userId: String,
        @RequestParam page: Int?
    ): ResponseEntity<BuyTokenHistoryList> {
        val transaction = buyTokenHistoryService.findAllByUserId(userId, page)
        return ResponseEntity.ok(BuyTokenHistoryList(transaction.map { BuyTokenTransactionResponse.of(it) }))
    }

    @GetMapping("/events-manager/buy-token-history/{eventId}")
    fun getMyScannerTransactionsHistory(
        principal: Principal,
        @PathVariable eventId: String,
        @RequestParam page: Int?
    ): ResponseEntity<BuyTokenHistoryList> {
        val eventOwnerId = getUserIdFrom(principal)
        val transactionList = buyTokenHistoryService.findAllPurchasedByEventAndOwner(eventOwnerId, eventId, page)

        return ResponseEntity.ok(BuyTokenHistoryList(transactionList.map { BuyTokenTransactionResponse.of(it) }))
    }

    @GetMapping("/my/buy-token-history/{transactionId}/details")
    fun getBuyTransactionHistoryDetails(
        @PathVariable transactionId: String,
        principal: Principal
    ): ResponseEntity<BuyTransactionDetails> {
        val requestingUserId = getUserIdFrom(principal)
        val transaction = buyTokenHistoryService.findById(transactionId, requestingUserId)

        return ResponseEntity.ok(BuyTransactionDetails.of(transaction))
    }
}

data class BuyTransactionDetails(
    val id: String,
    val userId: String,
    val tokens: List<GroupedTokenDetails>,
    val eventId: String,
    val eventName: String,
    val createdAt: Instant,
    var status: BuyTokenTransactionStatus
) {
    companion object {
        fun of(transaction: BuyTokenTransaction) = BuyTransactionDetails(
            id = transaction.id,
            userId = transaction.userId,
            tokens = GroupedTokenDetails.toGroupedTokenDetails(transaction.tokens),
            eventId = transaction.eventId,
            eventName = transaction.eventName,
            createdAt = transaction.createdAt,
            status = transaction.status
        )
    }
}

data class BuyTokenHistoryList(val list: List<BuyTokenTransactionResponse>)

data class BuyTokenTransactionResponse(
    val id: String,
    val userId: String,
    val eventName: String,
    val createdAt: Instant,
    var status: BuyTokenTransactionStatus,
    val totalValue: Long,
    val amount: Int
) {
    companion object {
        fun of(transaction: BuyTokenTransaction) = BuyTokenTransactionResponse(
            id = transaction.id,
            userId = transaction.userId,
            eventName = transaction.eventName,
            createdAt = transaction.createdAt,
            status = transaction.status,
            amount = transaction.tokens.size,
            totalValue = transaction.tokens.sumBy { it.tokenValue.toInt() }.toLong()
        )
    }
}

