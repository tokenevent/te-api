package pl.tokenevent.resources

import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.infra.getUserIdFrom
import pl.tokenevent.infra.users.User
import pl.tokenevent.infra.users.UserService
import pl.tokenevent.infra.users.UserWithTokens
import pl.tokenevent.infra.wallets.WalletService
import pl.tokenevent.resources.dto.WalletResponse
import java.security.Principal
import java.time.Duration
import java.time.Instant

@CrossOrigin(origins = ["http://localhost:3000", "http://te-web-client.herokuapp.com"])
@RestController
@RequestMapping("/users")
internal class UsersResources(private val userService: UserService) {

    @GetMapping("/current")
    fun getCurrent(@AuthenticationPrincipal user: User): ResponseEntity<User> {
        return ResponseEntity.ok(user)
    }

    @GetMapping("/me")
    fun getCurrent(
        principal: Principal
    ): ResponseEntity<UserWithTokens> {
        return ResponseEntity.ok(userService.getUserWithTokens(getUserIdFrom(principal)))
    }

    @GetMapping("/me/wallet")
    fun getWallet(
        principal: Principal
    ): ResponseEntity<WalletResponse> {
        val userId = getUserIdFrom(principal)

        val (id, _, createdAt) = userService.getWalletByUserId(userId)

        val timeLeft = WalletService.VALID_FOR - Duration.between(createdAt, Instant.now()).abs().seconds
        return ResponseEntity.ok(
            WalletResponse(
                id,
                timeLeft
            ))
    }

    @GetMapping("/{userId}")
    fun getUsersDetails(
        @PathVariable userId: String
    ): ResponseEntity<UserWithTokens> {
        return ResponseEntity.ok(userService.getUserWithTokens(userId))
    }

    @PutMapping("/my/settings")
    fun setMySettings(
        principal: Principal,
        @RequestBody request: EditSettingsRequest
    ): ResponseEntity<User> {
        val userId = getUserIdFrom(principal)

        return ResponseEntity.ok(userService.setSettings(userId, request))
    }
}

data class EditSettingsRequest(
    val email: String?,
    val phone: String?,
    val firstName: String?,
    val lastName: String?
)
