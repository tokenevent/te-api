package pl.tokenevent.resources;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.tokenevent.exceptions.ApiException;
import pl.tokenevent.exceptions.CredentialsException;
import pl.tokenevent.exceptions.UsernameTakenException;
import pl.tokenevent.infra.promocode.PromoCodeService;
import pl.tokenevent.infra.users.User;
import pl.tokenevent.infra.users.UserAuthenticationService;
import pl.tokenevent.infra.users.UserService;
import pl.tokenevent.resources.dto.LoginFormDTO;
import pl.tokenevent.resources.dto.UserRegisterRequest;
import pl.tokenevent.resources.dto.UserResponse;

import java.util.ArrayList;
import java.util.List;

import static pl.tokenevent.infra.Roles.ROLE_ADMIN;
import static pl.tokenevent.infra.Roles.ROLE_USER;

@CrossOrigin("*")
@RestController
@RequestMapping("/public/users")
class PublicResources {

    private final UserAuthenticationService authentication;

    private final UserService userService;

    private final PromoCodeService promoCodeService;

    @Autowired
    public PublicResources(UserAuthenticationService authentication, UserService userService, PromoCodeService promoCodeService) {
        this.authentication = authentication;
        this.userService = userService;
        this.promoCodeService = promoCodeService;
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponse> register(
        @RequestBody UserRegisterRequest userRegisterRequest
    ) {
        String promoCode = userRegisterRequest.getPromoCode();
        if (promoCode != null && !promoCodeService.isValidCode(promoCode)) {
            throw new ApiException(HttpStatus.BAD_REQUEST, "Promo code does not exist or is inactive.");
        }
        if (userService.findByUsername(userRegisterRequest.getUsername()).isPresent()) {
            throw new UsernameTakenException();
        }
        List<GrantedAuthority> authorities = Lists.newArrayList(new SimpleGrantedAuthority(ROLE_USER.toString()));

        User savedUser = userService.saveUser(User.of(userRegisterRequest, authorities));
        if (promoCode != null) {
            promoCodeService.usePromoCode(savedUser.getId(), promoCode);
        }
        return ResponseEntity.ok(new UserResponse(savedUser));
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(
        @RequestBody LoginFormDTO loginFormDTO
    ) {
        String username = loginFormDTO.getUsername();
        String password = loginFormDTO.getPassword();

        String token = authentication
            .login(username, password)
            .orElseThrow(CredentialsException::new);

        return ResponseEntity.ok(token);
    }

    @PostMapping("/registeradmin")
    public ResponseEntity<UserResponse> registerAdmin(@RequestBody LoginFormDTO loginFormDTO) {
        if (userService.findByUsername(loginFormDTO.getUsername()).isPresent()) {
            throw new UsernameTakenException();
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(ROLE_USER.toString()));
        authorities.add(new SimpleGrantedAuthority(ROLE_ADMIN.toString()));

        return ResponseEntity.ok(new UserResponse(userService.saveUser(User.adminOf(loginFormDTO, authorities))));
    }

    @GetMapping("/echo")
    public ResponseEntity<String> echo() {
        return new ResponseEntity<>("echo", HttpStatus.OK);
    }
}
