package pl.tokenevent.resources.eventmanager

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.exceptions.UsernameTakenException
import pl.tokenevent.infra.eventmanager.EventManagerFacade
import pl.tokenevent.infra.getUserIdFrom
import pl.tokenevent.infra.users.User
import pl.tokenevent.infra.users.UserService
import java.lang.invoke.MethodHandles
import java.security.Principal

@RestController
class ScannersResource(
    val eventManagerFacade: EventManagerFacade,
    val userService: UserService
) {

    @ResponseStatus(CREATED)
    @PostMapping("/events-manager/events/{eventId}/scanners")
    fun addNewScanner(
        @PathVariable eventId: String,
        @RequestBody request: AddNewScannerRequest,
        principal: Principal
    ): NewUserResponse {
        val userId = getUserIdFrom(principal)

        if (userService.findByUsername(request.username).isPresent) {
            throw UsernameTakenException()
        }

        val registeredUser = eventManagerFacade.createNewScanner(
            creatorId = userId,
            eventId = eventId,
            newUsername = request.username,
            newPassword = request.password
        )

        return NewUserResponse(registeredUser)
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/events-manager/events/{eventId}/scanners/{scannerId}")
    fun deleteScanner(
        principal: Principal,
        @PathVariable eventId: String,
        @PathVariable scannerId: String
    ) {
        val userId = getUserIdFrom(principal)
        logger.info("Deleting scanner $scannerId by $userId")

        eventManagerFacade.deleteScanner(
            parentId = userId,
            eventId = eventId,
            scannerId = scannerId
        )

        logger.debug("Deleted scanner: $scannerId")
    }

    @GetMapping("/events-manager/events/{eventId}/scanners")
    fun getMyScanners(
        principal: Principal,
        @PathVariable eventId: String
    ): Scanners {
        val creatorId = getUserIdFrom(principal)

        return Scanners(eventManagerFacade.getScannersByCreatorId(creatorId, eventId))
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }
}

data class AddNewScannerRequest(
    val username: String,
    val password: String
)

data class NewUserResponse(val user: User)

data class Scanners(val scanners: List<User>)
