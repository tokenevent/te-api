package pl.tokenevent.resources.eventmanager

import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.OK
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.domain.Event
import pl.tokenevent.infra.events.EventService
import pl.tokenevent.infra.getUserIdFrom
import pl.tokenevent.resources.dto.BuyableTokenRequest
import pl.tokenevent.resources.dto.EventRequest
import pl.tokenevent.resources.dto.UpdateEventRequest
import java.security.Principal

@CrossOrigin(origins = ["http://localhost:3000", "http://te-web-client.herokuapp.com"])
@RestController
class EventsResource(
    val eventService: EventService
) {

    @GetMapping("/events-manager/my/events")
    fun getMyEvents(principal: Principal): Events {
        val creatorId = getUserIdFrom(principal)
        return Events(eventService.findEventsByCreatorId(creatorId))
    }

    @PostMapping("/events-manager/events")
    fun addEvent(
        @RequestBody eventRequest: EventRequest,
        principal: Principal
    ): ResponseEntity<Event> {
        val creatorId = getUserIdFrom(principal)

        return ResponseEntity(eventService.save(eventRequest.toEvent(creatorId)), CREATED)
    }

    //todo update coords
    @PutMapping("/events-manager/events/{id}")
    fun updateEvent(
        @RequestBody updateEventRequest: UpdateEventRequest,
        @PathVariable id: String,
        principal: Principal
    ): ResponseEntity<Event> {
        val creatorId = getUserIdFrom(principal)

        return ResponseEntity(eventService.update(updateEventRequest, creatorId, id), OK)
    }

    @DeleteMapping("/events-manager/events/{id}")
    fun deleteEvent(@PathVariable id: String): ResponseEntity<Void> {
        eventService.delete(id)
        return ResponseEntity.ok().build()
    }

    @PutMapping("/events-manager/events/{id}/buyable-tokens")
    fun addBuyableToken(
        @PathVariable id: String,
        @RequestBody request: List<BuyableTokenRequest>,
        principal: Principal
    ): ResponseEntity<Event> {
        val requestingUserId = getUserIdFrom(principal)
        val buyableTokens = request.map { it.toBuyableToken() }.toHashSet()
        return ResponseEntity.ok(eventService.addBuyableToken(buyableTokens, id, requestingUserId))
    }

    @DeleteMapping("/events-manager/events/{id}/buyable-tokens/{name}")
    fun deleteBuyableToken(
        @PathVariable id: String,
        @PathVariable name: String,
        principal: Principal
    ) {
        val requestingUserId = getUserIdFrom(principal)
        eventService.deleteBuyableToken(requestingUserId, id, name)
    }

    @GetMapping("/events-manager/events/{eventId}/summary")
    fun getEventSummary(
        @PathVariable eventId: String,
        principal: Principal
    ): ResponseEntity<EventSummary> {
        val requestingUserId = getUserIdFrom(principal)
        val summary = eventService.getEventSummary(eventId, requestingUserId)
        return ResponseEntity.ok(summary)
    }

    @GetMapping("/events-manager/scanners/{scannerId}/summary")
    fun getScannerSummary(
        @PathVariable scannerId: String,
        principal: Principal
    ): ResponseEntity<EventSummary> {
        val requestingUserId = getUserIdFrom(principal)
        val summary = eventService.getScannerSummary(scannerId, requestingUserId)
        return ResponseEntity.ok(summary)
    }
}

data class Events(val events: List<Event>)

data class EventSummary(
    val transactions: TransactionsSummary,
    val uniqueUsers: Int,
    val tokens: TokensSummary
)

data class TransactionsSummary(
    val count: Int,
    val totalAmount: Long
)

data class TokensSummary(
    val tokens: List<SingleTokenSummary>
)

data class SingleTokenSummary(
    val buyableTokenName: String,
    val count: Int,
    val totalAmount: Long
)
