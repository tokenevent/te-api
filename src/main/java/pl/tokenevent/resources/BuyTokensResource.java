package pl.tokenevent.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.tokenevent.infra.UtilsKt;
import pl.tokenevent.infra.tokensIssue.TokenIssuerService;
import pl.tokenevent.resources.dto.IssueTokenRequest;
import pl.tokenevent.resources.dto.PayuNewTransactionResponse;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.security.Principal;

@CrossOrigin(origins = {"http://localhost:3000", "http://te-web-client.herokuapp.com"})
@RestController
public class BuyTokensResource {
    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final TokenIssuerService tokenIssuerService;

    public BuyTokensResource(TokenIssuerService tokenIssuerService) {
        this.tokenIssuerService = tokenIssuerService;
    }

    @PostMapping("/tokens/payu")
    public ResponseEntity<PayuNewTransactionResponse> issueTokenWithPayu(
        @RequestBody IssueTokenRequest request,
        Principal principal
    ) {
        String userId = UtilsKt.getUserIdFrom(principal);
        logger.info("User: {} wants to buy tokens with request {}", userId, request);

        PayuNewTransactionResponse locationAndTransactionId = tokenIssuerService.buyTokensUsingPayu(
            request.getEventId(),
            request.getTokensToBuy(),
            userId
        );
        return ResponseEntity.created(URI.create(locationAndTransactionId.getLocationUrl())).body(locationAndTransactionId);
    }
}
