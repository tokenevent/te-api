package pl.tokenevent.resources.dto

data class UserRegisterRequest(
    val username: String,
    val password: String,
    val email: String,
    val phone: String,
    val firstName: String,
    val lastName: String,
    val promoCode: String?
)
