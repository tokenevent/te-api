package pl.tokenevent.resources.dto

import pl.tokenevent.domain.Token

data class TokensDetails(
    val buyableTokenId: String,
    val buyableTokenName: String,
    val buyableTokenUnitPrice: Long,
    val scannedTokens: List<Token>
)
