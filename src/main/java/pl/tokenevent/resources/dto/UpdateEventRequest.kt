package pl.tokenevent.resources.dto

import pl.tokenevent.domain.BuyableToken
import java.time.Instant

data class UpdateEventRequest(
    val description: String? = null,
    val startingAt: Instant? = null,
    val endingAt: Instant? = null,
    val buyableTokens: Set<NewBuyableToken>? = null,
    val city: String? = null,
    val street: String? = null,
    val zipCode: String? = null
)

data class NewBuyableToken(
    val name: String,
    val unitPrice: Long
) {
    fun toBuyableToken(): BuyableToken = BuyableToken(name = name, unitPrice = unitPrice)
}
