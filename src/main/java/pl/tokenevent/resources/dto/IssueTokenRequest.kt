package pl.tokenevent.resources.dto

data class IssueTokenRequest(
    val eventId: String,
    val tokensToBuy: Set<TokenType>
)