package pl.tokenevent.resources.dto

data class TokenType(
    val id: String,
    val quantity: Int
)