package pl.tokenevent.resources.dto

class ImageBase64Request(
    val value: String? = null
)