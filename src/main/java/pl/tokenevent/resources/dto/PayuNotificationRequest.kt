package pl.tokenevent.resources.dto

import pl.tokenevent.infra.payments.dto.PayuNotificationProperties
import pl.tokenevent.infra.payments.dto.PayuOrder

data class PayuNotificationRequest(
    val order: PayuOrder,
    val localReceiptDateTime: String?,
    val properties: List<PayuNotificationProperties>?
)
