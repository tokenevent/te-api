package pl.tokenevent.resources.dto

import pl.tokenevent.domain.Event
import pl.tokenevent.domain.EventAddress
import pl.tokenevent.domain.EventType
import java.time.Instant

data class EventRequest(
    val type: EventType,
    val name: String,
    val description: String,
    val city: String,
    val street: String,
    val startingAt: Instant,
    val endingAt: Instant,
    val zipCode: String? = null
) {

    fun toEvent(creatorId: String): Event = Event(
        type = type,
        name = name,
        description = description,
        startingAt = startingAt,
        endingAt = endingAt,
        buyableTokens = emptySet(),
        creatorId = creatorId,
        address = EventAddress(city = city, street = street, zipCode = zipCode)
    )
}
