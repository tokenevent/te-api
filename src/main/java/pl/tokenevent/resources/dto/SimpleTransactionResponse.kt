package pl.tokenevent.resources.dto

import pl.tokenevent.domain.Transaction
import pl.tokenevent.domain.TransactionStatus
import java.time.Instant

data class SimpleTransactionResponse(
    val transactions: List<TransactionResponseItem>
)

data class TransactionResponseItem(
    val id: String,
    val scannerUserId: String,
    val userId: String,
    val eventId: String,
    val eventName: String,
    val transactionAt: Instant,
    val tokens: TransactionTokens,
    val status: TransactionStatus,
    val statusChangedAt: Instant?,
    val userName: String,
    val scannerUserName: String
) {
    companion object {
        fun of(transaction: Transaction): TransactionResponseItem {
            with(transaction) {
                return TransactionResponseItem(
                    id,
                    scannerUserId,
                    userId,
                    eventId,
                    eventName,
                    transactionAt,
                    TransactionTokens(
                        tokens.size,
                        tokens.map { it.tokenValue }.reduce { a, b -> (a + b) }
                    ),
                    status,
                    statusChangedAt,
                    userName,
                    scannerUserName
                )
            }
        }
    }
}

data class TransactionTokens(
    val count: Int,
    val totalValue: Long
)
