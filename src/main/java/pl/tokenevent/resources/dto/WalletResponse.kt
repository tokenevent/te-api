package pl.tokenevent.resources.dto

data class WalletResponse(
    val id: String,
    val validForSeconds: Long
)
