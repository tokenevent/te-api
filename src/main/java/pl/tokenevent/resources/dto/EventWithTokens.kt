package pl.tokenevent.resources.dto

import pl.tokenevent.domain.Event
import pl.tokenevent.domain.Token

data class EventWithTokens(
    val event: Event,
    val tokens: List<Token>
)
