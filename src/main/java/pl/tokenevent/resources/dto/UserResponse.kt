package pl.tokenevent.resources.dto

import pl.tokenevent.infra.users.User

data class UserResponse(
    val user: User
)