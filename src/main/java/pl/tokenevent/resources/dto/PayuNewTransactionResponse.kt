package pl.tokenevent.resources.dto

data class PayuNewTransactionResponse(
    val locationUrl: String,
    val transactionId: String
)
