package pl.tokenevent.resources.dto

data class LoginFormDTO(
    val username: String,
    val password: String
)