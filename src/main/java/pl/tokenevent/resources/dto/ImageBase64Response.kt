package pl.tokenevent.resources.dto

data class ImageBase64Response(
    val id: String,
    val value: String
)