package pl.tokenevent.resources.dto

import pl.tokenevent.domain.BuyableToken

data class BuyableTokenRequest(
    val name: String,
    val unitPrice: Long
) {
    fun toBuyableToken(): BuyableToken {
        return BuyableToken(name = name, unitPrice = unitPrice)
    }
}
