package pl.tokenevent.resources;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.tokenevent.domain.Event;
import pl.tokenevent.exceptions.ResourceNotFoundException;
import pl.tokenevent.infra.events.EventService;

/*
    public resource for user
 */
@CrossOrigin(origins = {"http://localhost:3000", "http://te-web-client.herokuapp.com"})
@RestController
@RequestMapping("/events")
public class EventsResources {
    private EventService eventService;

    public EventsResources(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Event> getEvent(@PathVariable String id) {
        Event event = eventService.getEvent(id).orElseThrow(ResourceNotFoundException::new);
        return new ResponseEntity<>(event, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Iterable<Event>> getAllEvents() {
        return new ResponseEntity<>(eventService.findAll(), HttpStatus.OK);
    }

}
