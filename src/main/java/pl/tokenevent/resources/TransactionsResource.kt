package pl.tokenevent.resources

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.domain.Transaction
import pl.tokenevent.domain.TransactionStatus
import pl.tokenevent.exceptions.NotEnoughTokensException
import pl.tokenevent.infra.getUserIdFrom
import pl.tokenevent.infra.transactions.TransactionService
import java.lang.invoke.MethodHandles
import java.security.Principal

@CrossOrigin(origins = ["http://localhost:3000", "http://te-web-client.herokuapp.com"])
@RestController
class TransactionsResource(
    private val transactionService: TransactionService
) {

    /**
     * endpoint for scanner create new transaction
     */
    @PostMapping("/scanner/transactions")
    fun useTokens(
        @RequestBody request: UseTokensRequest,
        principal: Principal
    ): ResponseEntity<TransactionResponse> {
        val scanningUserId = getUserIdFrom(principal)

        logger.info("Scanner with id {} used tokens", scanningUserId)

        val transaction = transactionService.createTransaction(
            request.walletId,
            request.tokens,
            scanningUserId
        )

        return ResponseEntity.ok(TransactionResponse(transaction))
    }

    /**
     * endpoint for scanner to get user transactions
     */
    @GetMapping("/scanner/transactions/{id}")
    fun getTransactionById(
        @PathVariable id: String,
        principal: Principal
    ): TransactionResponse {
        logger.info("Scanner polled for $id")
        val transaction = transactionService.getTransaction(id)
        logger.info("Scanner returning ${transaction.id}")
        return TransactionResponse(transaction)
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/scanner/transactions/{id}")
    fun rejectPendingTransaction(
        @PathVariable id: String,
        principal: Principal
    ) {
        val scannerId = getUserIdFrom(principal)
        transactionService.rejectPendingTransaction(scannerId, id)
    }

    /**
     * endpoint for user to get his transactions
     */
    @GetMapping("/my/transactions")
    fun getUserTransaction(
        principal: Principal,
        @RequestParam page: Int?
    ): TransactionResponse {
        val userId = getUserIdFrom(principal)

        val transaction = transactionService.getTransactionPendingByUserId(userId, page)
        logger.info("Transaction status: {}, eventId: {}", transaction.status, transaction.eventId)
        if (transaction.eventId == "ERROR_SCAN_NO_TOKENS") {
            transactionService.delete(transaction)
            logger.info("Deleting {} transaction due to no enough tokens", userId)
            throw NotEnoughTokensException()
        }
        return TransactionResponse(transaction)
    }

    /**
     * endpoint for user accept/reject pending transaction
     */
    @PostMapping("/my/transactions")
    @ResponseBody
    fun setTransactionStatus(
        principal: Principal,
        @RequestBody request: SetTransactionStatusRequest,
        @RequestParam page: Int?
    ): TransactionResponse {
        val userId = getUserIdFrom(principal)
        val newStatus = request.decision.toTransactionStatus()
        val transaction = transactionService.setTransactionStatus(userId, request.transactionId, newStatus, page)

        return TransactionResponse(transaction)
    }

    data class UseTokensRequest(
        val walletId: String,
        val tokens: Set<TokensToBuy>
    )

    data class TokensToBuy(
        val id: String,
        val quantity: Int
    )

    data class TransactionResponse(
        val transaction: Transaction,
        val error: Error? = null
    )

    data class Error(
        val reason: String
    )

    data class SetTransactionStatusRequest(
        val transactionId: String,
        val decision: SetTransactionDecision
    )

    enum class SetTransactionDecision {
        ACCEPT {
            override fun toTransactionStatus(): TransactionStatus = TransactionStatus.ACCEPTED
        },
        REJECT {
            override fun toTransactionStatus(): TransactionStatus = TransactionStatus.REJECTED
        };

        abstract fun toTransactionStatus(): TransactionStatus
    }

    companion object {
        private val logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }
}

