package pl.tokenevent.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.tokenevent.infra.tokensIssue.TokenIssuerService;
import pl.tokenevent.resources.dto.PayuNotificationRequest;

import java.lang.invoke.MethodHandles;

@CrossOrigin(origins = {"http://localhost:3000", "http://te-web-client.herokuapp.com"})
@RestController
public class PayuNotificationResource {
    private Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private TokenIssuerService tokenIssuerService;

    public PayuNotificationResource(TokenIssuerService tokenIssuerService) {
        this.tokenIssuerService = tokenIssuerService;
    }

    @PostMapping("/payu/notify")
    public ResponseEntity<Object> issueTokenWithPayu(
        @RequestBody PayuNotificationRequest request
    ) {

        logger.info("Notify from Payu, {}", request);
        tokenIssuerService.processPayNotification(request);

        return ResponseEntity.ok().build();
    }
}
