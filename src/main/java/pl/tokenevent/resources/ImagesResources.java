package pl.tokenevent.resources;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.tokenevent.domain.Image;
import pl.tokenevent.infra.images.ImagesService;
import pl.tokenevent.resources.dto.ImageBase64Request;
import pl.tokenevent.resources.dto.ImageBase64Response;

import java.security.Principal;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static pl.tokenevent.infra.UtilsKt.getUserIdFrom;

@CrossOrigin(origins = {"http://localhost:3000", "http://te-web-client.herokuapp.com"})
@RestController
@RequestMapping("/images")
public class ImagesResources {

    private ImagesService imagesService;

    public ImagesResources(ImagesService imagesService) {
        this.imagesService = imagesService;
    }

    @PostMapping
    public ResponseEntity<Void> addImage(
        @RequestBody ImageBase64Request imageBase64,
        Principal principal,
        UriComponentsBuilder uriComponentsBuilder
    ) {
        Image image = imagesService.save(imageBase64.getValue(), getUserIdFrom(principal));

        UriComponents uriComponents =
            uriComponentsBuilder.path("/images/{id}").buildAndExpand(image.getId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @GetMapping(
        value = "/{id}")
    public ResponseEntity<ImageBase64Response> getImage(@PathVariable String id) {
        Image image = imagesService.get(id);
        return new ResponseEntity<>(new ImageBase64Response(image.getId(), image.getBase64value()), OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteImage(
        @PathVariable String id,
        Principal principal
    ) {
        imagesService.delete(id, getUserIdFrom(principal));
    }
}
