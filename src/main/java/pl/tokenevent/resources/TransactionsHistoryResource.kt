package pl.tokenevent.resources

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.tokenevent.domain.TransactionDetails
import pl.tokenevent.infra.getUserIdFrom
import pl.tokenevent.infra.transactions.TransactionService
import pl.tokenevent.resources.dto.SimpleTransactionResponse
import pl.tokenevent.resources.dto.TransactionResponseItem
import java.security.Principal

@CrossOrigin(origins = ["http://localhost:3000", "http://te-web-client.herokuapp.com"])
@RestController
class TransactionsHistoryResource(
    val transactionService: TransactionService
) {

    /*
    endpoint for user to get his transactions history
     */
    @GetMapping("/my/transactions-history")
    fun getMyTransactions(
        principal: Principal,
        @RequestParam page: Int?
    ): ResponseEntity<SimpleTransactionResponse> {
        val userId = getUserIdFrom(principal)
        val pageRequest = PageRequest.of(page ?: 0, 20, Sort.by("transactionAt").descending())
        return ResponseEntity.ok(
            SimpleTransactionResponse(
                transactionService.findAllByUserId(userId, pageRequest)
                    .map { TransactionResponseItem.of(it) }
            ))
    }

    @GetMapping("/admin/transactions-history")
    fun getAllTransactions(principal: Principal): ResponseEntity<SimpleTransactionResponse> {
        return ResponseEntity.ok(SimpleTransactionResponse(transactionService.findAll().map { TransactionResponseItem.of(it) }))
    }

    @GetMapping("/scanner/transactions-history")
    fun getScannerTransactions(
        principal: Principal,
        @RequestParam page: Int?
    ): ResponseEntity<SimpleTransactionResponse> {
        val scannerId = getUserIdFrom(principal)
        return ResponseEntity.ok(SimpleTransactionResponse(transactionService.findAllByScannerUserId(scannerId, page).map { TransactionResponseItem.of(it) }))
    }

    @GetMapping("/admin/transactions-history/{userId}")
    fun getUsersTransactions(
        principal: Principal,
        @PathVariable userId: String
    ): ResponseEntity<SimpleTransactionResponse> {
        return ResponseEntity.ok(SimpleTransactionResponse(transactionService.findAllByUserId(userId)
            .map { TransactionResponseItem.of(it) }))
    }

    @GetMapping("/events-manager/events/{eventId}/transactions-history/")
    fun getTransactionHistoryByEventId(
        @PathVariable eventId: String,
        principal: Principal,
        @RequestParam page: Int?
    ): ResponseEntity<SimpleTransactionResponse> {
        val creatorId = getUserIdFrom(principal)
        return ResponseEntity.ok(SimpleTransactionResponse(transactionService.findAllByEventId(creatorId, eventId, page)
            .map { TransactionResponseItem.of(it) }
        ))
    }

    @GetMapping("/events-manager/transactions-history/{scannerId}")
    fun getMyScannerTransactionsHistory(
        principal: Principal,
        @PathVariable scannerId: String,
        @RequestParam page: Int?
    ): ResponseEntity<SimpleTransactionResponse> {
        val creatorId = getUserIdFrom(principal)
        return ResponseEntity.ok(SimpleTransactionResponse(transactionService.findAllByCreatorAndScannerId(creatorId, scannerId, page)
            .map { TransactionResponseItem.of(it) }
        ))
    }

    @GetMapping("/transaction-details/{transactionId}")
    fun getTransactionDetails(
        @PathVariable transactionId: String,
        principal: Principal
    ): ResponseEntity<TransactionDetails> {

        return ResponseEntity.ok(transactionService.findDetailsById(transactionId))
    }
}
