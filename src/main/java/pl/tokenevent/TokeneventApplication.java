package pl.tokenevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TokeneventApplication {
    public static void main(String[] args) {
        SpringApplication.run(TokeneventApplication.class, args);
    }
}
