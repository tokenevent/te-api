package pl.tokenevent.domain

import java.time.Instant

data class TransactionDetails(
    val status: TransactionStatus,
    val scannerName: String,
    val userName: String,
    val eventName: String,
    val transactionAt: Instant,
    val statusChangedAt: Instant?,
    val tokens: List<GroupedTokenDetails>
) {
    companion object {
        fun of(transaction: Transaction) = TransactionDetails(
            status = transaction.status,
            scannerName = transaction.scannerUserName,
            userName = transaction.userName,
            eventName = transaction.eventName,
            transactionAt = transaction.transactionAt,
            statusChangedAt = transaction.statusChangedAt,
            tokens = GroupedTokenDetails.toGroupedTokenDetails(transaction.tokens)
        )


    }
}

data class GroupedTokenDetails(
    val buyableTokenName: String,
    val totalValue: Long,
    val unitPrice: Long,
    val amount: Long
) {
    companion object {
        fun toGroupedTokenDetails(tokens: List<Token>): List<GroupedTokenDetails> = tokens
            .groupBy { it.buyableTokenName }
            .map { entry ->
                GroupedTokenDetails(
                    buyableTokenName = entry.key,
                    totalValue = entry.value.sumBy { it.tokenValue.toInt() }.toLong(),
                    unitPrice = entry.value.first().tokenValue,
                    amount = entry.value.size.toLong()
                )
            }
    }
}
