package pl.tokenevent.domain

enum class EventType {
    VALUE, ITEM
}
