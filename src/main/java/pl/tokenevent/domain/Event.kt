package pl.tokenevent.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.util.UUID

@Document(collection = "events")
data class Event(
    @Indexed @Id val id: String = UUID.randomUUID().toString(),
    val type: EventType,
    val name: String,
    var description: String? = null,
    var startingAt: Instant,
    var endingAt: Instant,
    var buyableTokens: Set<BuyableToken>,
    @Indexed val creatorId: String,
    var address: EventAddress
) {
    val createdAt: Instant = Instant.now()
}
