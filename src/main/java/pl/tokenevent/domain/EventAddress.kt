package pl.tokenevent.domain

data class EventAddress(
    var city: String,
    var street: String,
    var zipCode: String? = null,
    var coordinates: Coordinates? = null
) {
    fun toFullAddress(): String {
        return "$city $street $zipCode".replace("\\s".toRegex(), "+")
    }
}
