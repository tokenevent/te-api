package pl.tokenevent.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

import java.time.Instant

@Document(collection = "tokens")
class Token(
    @Id @Indexed val id: String,
    @Indexed val ownerId: String,
    var status: TokenStatus,
    val eventId: String,
    val eventName: String,
    val buyableTokenId: String,
    val buyableTokenName: String,
    val tokenValue: Long
) {
    val createdAt: Instant = Instant.now()
    var lastChangeAt: Instant = Instant.now()
}

