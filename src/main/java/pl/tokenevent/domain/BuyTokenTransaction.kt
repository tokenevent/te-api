package pl.tokenevent.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(collection = "BuyTokenTransaction")
data class BuyTokenTransaction(
    @Indexed @Id val id: String,
    @Indexed val userId: String,
    val tokens: List<Token>,
    @Indexed val eventId: String,
    val eventName: String,
    @Indexed val createdAt: Instant = Instant.now(),
    var status: BuyTokenTransactionStatus
)

enum class BuyTokenTransactionStatus {
    PENDING, WAITING_FOR_CONFIRMATION, COMPLETED, CANCELED, PROMOTION
}
