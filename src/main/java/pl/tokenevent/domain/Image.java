package pl.tokenevent.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

public class Image {
    @Id
    @Indexed
    private String id;
    private String base64value;
    private String creatorUserId;

    public Image(String base64value, String creatorUserId) {
        this.base64value = base64value;
        this.creatorUserId = creatorUserId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBase64value() {
        return base64value;
    }

    public void setBase64value(String base64value) {
        this.base64value = base64value;
    }

    public String getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(String creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    @Override
    public String toString() {
        return "Image{" +
            "id='" + id + '\'' +
            ", base64value='" + base64value + '\'' +
            ", creatorUserId='" + creatorUserId + '\'' +
            '}';
    }
}
