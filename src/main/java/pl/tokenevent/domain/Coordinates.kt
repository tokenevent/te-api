package pl.tokenevent.domain

data class Coordinates(
    val lat: String,
    val lon: String
)
