package pl.tokenevent.domain

import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "promotions")
data class Promotion(
    @Indexed val code: String,
    val type: PromotionType,
    val eventId: String,
    val isActive: Boolean
)

enum class PromotionType { ONE_OF_EACH }
