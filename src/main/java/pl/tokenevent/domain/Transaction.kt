package pl.tokenevent.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import pl.tokenevent.domain.TransactionStatus.PENDING
import pl.tokenevent.domain.TransactionStatus.REJECTED
import java.time.Instant
import java.util.UUID

@Document
data class Transaction(
    @Id
    @Indexed val id: String,
    @Indexed val scannerUserId: String,
    val scannerUserName: String,
    @Indexed val userId: String,
    val userName: String,
    @Indexed val eventId: String,
    val eventName: String,
    val tokens: List<Token>,
    @Indexed var status: TransactionStatus,
    val transactionAt: Instant = Instant.now(),
    var statusChangedAt: Instant? = null
) {
    companion object {
        fun fakePendingTransaction(): Transaction = Transaction(
            id = UUID.randomUUID().toString(),
            scannerUserId = UUID.randomUUID().toString(),
            userId = UUID.randomUUID().toString(),
            eventId = UUID.randomUUID().toString(),
            eventName = "Nazwa wydarzenia",
            tokens = emptyList(),
            status = PENDING,
            userName = UUID.randomUUID().toString(),
            scannerUserName = UUID.randomUUID().toString()
        )

        fun fakePendingNotEnoughTokensTransaction(userId: String): Transaction = Transaction(
            id = UUID.randomUUID().toString(),
            scannerUserId = UUID.randomUUID().toString(),
            userId = userId,
            eventId = "ERROR_SCAN_NO_TOKENS",
            eventName = "Nazwa wydarzenia",
            tokens = emptyList(),
            status = PENDING,
            userName = UUID.randomUUID().toString(),
            scannerUserName = UUID.randomUUID().toString()
        )
    }
}

enum class TransactionStatus {
    PENDING, ACCEPTED, REJECTED
}
