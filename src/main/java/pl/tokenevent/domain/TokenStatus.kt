package pl.tokenevent.domain

enum class TokenStatus {
    USABLE, RESERVED, USED
}
