package pl.tokenevent.domain

import java.util.UUID

data class BuyableToken(
    val id: String = UUID.randomUUID().toString(),
    val name: String,
    val unitPrice: Long
)
