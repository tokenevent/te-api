package pl.tokenevent.exceptions;

import org.springframework.http.HttpStatus;

public class AccessException extends ApiException {
    private static HttpStatus status = HttpStatus.FORBIDDEN;
    private static String message = "You're not allowed to do this. ";

    public AccessException(String failMessage) {
        super(status, message + failMessage);
    }
}
