package pl.tokenevent.exceptions;

import org.springframework.http.HttpStatus;

public class CredentialsException extends ApiException {
    private static HttpStatus status = HttpStatus.UNAUTHORIZED;
    private static String message = "Bad login and/or password";

    public CredentialsException() {
        super(status, message);
    }
}
