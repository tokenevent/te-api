package pl.tokenevent.exceptions

import org.springframework.http.HttpStatus

open class TransactionException(customMessage: String? = null, id: String) :
    ApiException(HttpStatus.CONFLICT, customMessage ?: "Transaction with id: $id does not exist")

class PendingTransactionNotFound : TransactionException("That user has no pending transaction", "")

class TooManyPendingTransaction : TransactionException("That user has more than one pending transaction", "")

class NotEnoughTokensException : TransactionException("That user doesn't have enough tokens", "")
