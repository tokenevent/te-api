package pl.tokenevent.exceptions;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.tokenevent.exceptions.errors.ErrorsHolder;

import java.lang.invoke.MethodHandles;

@ControllerAdvice
public class CustomExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @ExceptionHandler(CredentialsException.class)
    public ResponseEntity<ErrorsHolder> handleBadCredentials(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorsHolder> handleResourceNotFound(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(UsernameTakenException.class)
    public ResponseEntity<ErrorsHolder> handleUsernameTaken(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(BuyableTokenNotExists.class)
    public ResponseEntity<ErrorsHolder> handleBuyableTokenNotExists(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(WalletNotFoundException.class)
    public ResponseEntity<ErrorsHolder> handleWalletNotExists(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(AccessException.class)
    public ResponseEntity<ErrorsHolder> handleAccessForbidden(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(TransactionException.class)
    public ResponseEntity<ErrorsHolder> handleTransactionNotExists(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(EventManagerException.class)
    public ResponseEntity<ErrorsHolder> handleEventManagerExceptions(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ErrorsHolder> handleApiException(ApiException exception) {
        logger.error(exception.getMessage() + ". Exception: " + exception);

        return handleError(exception.getStatus(), exception.getMessage());
    }

    private ResponseEntity<ErrorsHolder> handleError(HttpStatus status, String message) {
        return new ResponseEntity<>(new ErrorsHolder(status, message), status);
    }

}
