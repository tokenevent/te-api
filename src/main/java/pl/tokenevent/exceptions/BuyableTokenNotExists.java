package pl.tokenevent.exceptions;

import org.springframework.http.HttpStatus;

public class BuyableTokenNotExists extends ApiException {
    private static HttpStatus status = HttpStatus.BAD_REQUEST;
    private static String message = "Could not find requested buyable token id";

    public BuyableTokenNotExists() {
        super(status, message);
    }
}
