package pl.tokenevent.exceptions;

import org.springframework.http.HttpStatus;

public class UsernameTakenException extends ApiException {
    private static HttpStatus status = HttpStatus.BAD_REQUEST;
    private static String message = "Username is taken";

    public UsernameTakenException() {
        super(status, message);
    }
}
