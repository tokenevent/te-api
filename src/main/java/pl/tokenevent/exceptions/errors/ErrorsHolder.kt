package pl.tokenevent.exceptions.errors

import org.springframework.http.HttpStatus

data class ErrorsHolder(
    val status: HttpStatus,
    val message: String
)