package pl.tokenevent.exceptions;

import org.springframework.http.HttpStatus;

public class ResourceNotFoundException extends ApiException {
    private static HttpStatus status = HttpStatus.NOT_FOUND;
    private static String message = "Requested resource has not been found";

    public ResourceNotFoundException() {
        super(status, message);
    }
}
