package pl.tokenevent.exceptions

import org.springframework.http.HttpStatus

class UserNotFoundException : ApiException(HttpStatus.UNAUTHORIZED, "Invalid principal")
