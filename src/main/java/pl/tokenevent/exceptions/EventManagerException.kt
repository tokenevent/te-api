package pl.tokenevent.exceptions

import org.springframework.http.HttpStatus

class EventManagerException(status: HttpStatus, message: String) : ApiException(status, message)
