package pl.tokenevent.exceptions;

import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

public class WalletNotFoundException extends ApiException {
    private static HttpStatus status = NOT_FOUND;
    private static String message = "Wallet not found.";

    public WalletNotFoundException() {
        super(status, message);
    }
}
