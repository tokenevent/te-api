package pl.tokenevent.infra.tokensIssue

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.stereotype.Service
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import pl.tokenevent.domain.BuyTokenTransaction
import pl.tokenevent.domain.BuyTokenTransactionStatus.CANCELED
import pl.tokenevent.domain.BuyTokenTransactionStatus.COMPLETED
import pl.tokenevent.domain.BuyTokenTransactionStatus.PENDING
import pl.tokenevent.domain.BuyTokenTransactionStatus.PROMOTION
import pl.tokenevent.domain.BuyableToken
import pl.tokenevent.domain.Event
import pl.tokenevent.domain.PromotionType
import pl.tokenevent.domain.Token
import pl.tokenevent.domain.TokenStatus.USABLE
import pl.tokenevent.exceptions.ApiException
import pl.tokenevent.exceptions.BuyableTokenNotExists
import pl.tokenevent.infra.events.EventService
import pl.tokenevent.infra.payments.PayuClient
import pl.tokenevent.infra.payments.dto.PayuBuyer
import pl.tokenevent.infra.payments.dto.PayuNewOrderRequest
import pl.tokenevent.infra.payments.dto.PayuProduct
import pl.tokenevent.infra.payments.dto.PayuSettings
import pl.tokenevent.infra.payments.dto.PayuStatus
import pl.tokenevent.infra.users.User
import pl.tokenevent.infra.users.UserService
import pl.tokenevent.repositories.BuyTokenTransactionsRepository
import pl.tokenevent.repositories.TokenRepository
import pl.tokenevent.resources.dto.PayuNewTransactionResponse
import pl.tokenevent.resources.dto.PayuNotificationRequest
import pl.tokenevent.resources.dto.TokenType
import java.lang.invoke.MethodHandles
import java.time.Instant
import java.util.UUID

@Service
class TokenIssuerService(
    private val eventService: EventService,
    private val userService: UserService,
    private val tokenRepository: TokenRepository,
    private val payuClient: PayuClient,
    private val buyTokenTransactionsRepository: BuyTokenTransactionsRepository
) {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
        const val MAX_TOKENS_TO_BUY_AT_ONCE = 1000
    }

    fun buyTokensUsingPayu(eventId: String, tokensToBuy: MutableSet<TokenType>, userId: String): PayuNewTransactionResponse {
        val size = tokensToBuy.size
        if (size > MAX_TOKENS_TO_BUY_AT_ONCE) {
            throw ApiException(BAD_REQUEST, "You're trying to buy to buy $size, max allowed is $MAX_TOKENS_TO_BUY_AT_ONCE")
        }

        val event = eventService.getEvent(eventId).orElseThrow { ApiException(NOT_FOUND, "Can't buy tokens, Event not found") }
        val user = userService.getUser(userId).orElseThrow { ApiException(NOT_FOUND, "Can't buy tokens, bad credentials") }
        val buyableTokens = event.buyableTokens

        val request = (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request
        val ip = request.remoteAddr

        //TODO popraw to Karol
        val tokens = mutableListOf<Token>()
        tokensToBuy.map { tokenToBuy ->
            createToken(buyableTokens, tokenToBuy, event, user, tokens)
        }

        val totalAmount = tokens.map { it.tokenValue }.reduce { acc, d -> d + acc }
        val payuProducts = tokens
            .groupBy { it.buyableTokenId }
            .map { map ->
                PayuProduct(
                    map.value[0].buyableTokenName,
                    map.value[0].tokenValue,
                    map.value.size
                )
            }

        val extOrderId = UUID.randomUUID().toString()
        //todo wywalić to do propertiesow
        val appUrl = "http://tokeneventapp.herokuapp.com"
        val order = PayuNewOrderRequest(
            notifyUrl = "$appUrl/payu/notify",
            customerIp = ip,
            totalAmount = totalAmount,
            extOrderId = extOrderId,
            buyer = PayuBuyer(
                email = user.account.email,
                phone = user.account.phone,
                firstName = user.account.firstName,
                lastName = user.account.lastName),
            settings = PayuSettings(),
            products = payuProducts
        )
        val payuOrder = payuClient.newOrder(order)

        val transaction = BuyTokenTransaction(
            id = extOrderId,
            userId = userId,
            tokens = tokens,
            eventId = eventId,
            eventName = event.name,
            status = PENDING
        )
        buyTokenTransactionsRepository.save(transaction)

        return PayuNewTransactionResponse(payuOrder.redirectUri, extOrderId)
    }

    fun processPayNotification(request: PayuNotificationRequest) {
        val extOrderId = request.order.extOrderId
        when (request.order.status) {
            PayuStatus.PENDING -> logger.info("Transaction is pending.")
            PayuStatus.WAITING_FOR_CONFIRMATION -> logger.info("Waiting for confirmation {}", extOrderId)
            PayuStatus.COMPLETED -> {
                handleCompletedNotification(extOrderId)
            }
            PayuStatus.CANCELED -> {
                handleCanceledNotification(extOrderId)
            }
        }
    }

    fun issueTokensWithPromoCode(userId: String, eventId: String, promoType: PromotionType) {
        val event = eventService.getEvent(eventId).orElseThrow { throw ApiException(NOT_FOUND, "Can't buy tokens, Event not found") }
        val user = userService.getUser(userId).orElseThrow { throw ApiException(NOT_FOUND, "Can't buy tokens, bad credentials") }
        val buyableTokens = event.buyableTokens

        when (promoType) {
            PromotionType.ONE_OF_EACH -> {
                val tokens = issueOneOfEach(buyableTokens, user, event)
                buyTokenTransactionsRepository.save(BuyTokenTransaction(
                    id = UUID.randomUUID().toString(),
                    userId = user.id,
                    tokens = tokens,
                    eventId = event.id,
                    eventName = event.name,
                    createdAt = Instant.now(),
                    status = PROMOTION
                ))
            }
        }

    }

    private fun issueOneOfEach(buyableTokens: Set<BuyableToken>, user: User, event: Event): List<Token> =
        tokenRepository.saveAll(buyableTokens.map { buyableToken ->
            Token(
                UUID.randomUUID().toString(),
                user.id,
                USABLE,
                event.id,
                event.name,
                buyableToken.id,
                buyableToken.name,
                buyableToken.unitPrice
            )
        })

    private fun handleCompletedNotification(extOrderId: String) {
        buyTokenTransactionsRepository.findById(extOrderId).ifPresent {
            if (it.status == PENDING) {
                it.status = COMPLETED
                tokenRepository.saveAll(it.tokens)
                buyTokenTransactionsRepository.save(it)
                logger.info("{} tokens added, buyTokenTransactionId {}", it.tokens.size, it.id)
            } else {
                logger.info("Completed transaction notification but transaction has status {}", it.status)
            }
        }
    }

    private fun handleCanceledNotification(extOrderId: String) {
        buyTokenTransactionsRepository.findById(extOrderId).ifPresent {
            if (it.status == PENDING) {
                it.status = CANCELED
                buyTokenTransactionsRepository.save(it)
                logger.info("Transaction canceled, buyTokenTransactionId {}", it.id)
            } else {
                logger.info("Transaction canceled but it has {}", it.status)
            }
        }
    }

    private fun createToken(buyableTokens: Set<BuyableToken>, tokenToBuy: TokenType, event: Event, user: User, tokens: MutableList<Token>) {
        val buyableToken = buyableTokens.find { it.id == tokenToBuy.id } ?: throw BuyableTokenNotExists()

        repeat(tokenToBuy.quantity) {
            val newToken = createToken(
                event,
                user,
                buyableToken.unitPrice,
                buyableToken.id,
                buyableToken.name
            )
            tokens.add(newToken)
        }
    }

    private fun createToken(event: Event, user: User, tokenValue: Long, buyableTokenId: String, buyableTokenName: String): Token {
        return Token(
            UUID.randomUUID().toString(),
            user.id,
            USABLE,
            event.id,
            event.name,
            buyableTokenId,
            buyableTokenName,
            tokenValue
        )
    }
}
