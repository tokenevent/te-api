package pl.tokenevent.infra

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import pl.tokenevent.exceptions.UserNotFoundException
import pl.tokenevent.infra.users.User
import java.security.Principal

fun getUserIdFrom(principal: Principal): String {
    return ((principal as UsernamePasswordAuthenticationToken).principal as User).id ?: throw UserNotFoundException()
}
