package pl.tokenevent.infra

enum class Roles {
    ROLE_ADMIN, ROLE_USER, ROLE_SCANNER, ROLE_EVENT_MANAGER
}
