package pl.tokenevent.infra.transactions

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import pl.tokenevent.domain.Token
import pl.tokenevent.domain.TokenStatus
import pl.tokenevent.domain.Transaction
import pl.tokenevent.domain.TransactionDetails
import pl.tokenevent.domain.TransactionStatus
import pl.tokenevent.domain.TransactionStatus.ACCEPTED
import pl.tokenevent.domain.TransactionStatus.PENDING
import pl.tokenevent.domain.TransactionStatus.REJECTED
import pl.tokenevent.exceptions.ApiException
import pl.tokenevent.exceptions.NotEnoughTokensException
import pl.tokenevent.exceptions.PendingTransactionNotFound
import pl.tokenevent.exceptions.TooManyPendingTransaction
import pl.tokenevent.exceptions.TransactionException
import pl.tokenevent.infra.users.UserRepository
import pl.tokenevent.infra.wallets.WalletService
import pl.tokenevent.repositories.TokenRepository
import pl.tokenevent.repositories.TransactionRepository
import pl.tokenevent.resources.TransactionsResource
import java.lang.invoke.MethodHandles
import java.time.Duration
import java.time.Instant
import java.util.UUID

@Service
class TransactionService(
    val transactionRepository: TransactionRepository,
    val userRepository: UserRepository,
    val walletService: WalletService,
    val tokenRepository: TokenRepository
) {
    fun findAll(): List<Transaction> = transactionRepository.findAll()

    fun findAllByScannerUserId(id: String, pageNumber: Int?): List<Transaction> {
        val page = PageRequest.of(pageNumber ?: 0, REQUEST_SIZE, Sort.by("transactionAt").descending())
        return transactionRepository.findAllByScannerUserId(id, page)
    }

    fun findAllByUserId(id: String): List<Transaction> {
        return transactionRepository.findAllByUserId(id)
    }

    fun findAllByUserId(id: String, page: PageRequest): List<Transaction> {
        return transactionRepository.findAllByUserId(id, page)
    }

    fun findAllByCreatorAndScannerId(creatorId: String, scannerId: String, pageNumber: Int?): List<Transaction> {
        val scanner = userRepository.findById(scannerId).orElseThrow { throw ApiException(NOT_FOUND, "Scanner not found") }
        if (scanner.parentId != creatorId) {
            throw ApiException(HttpStatus.FORBIDDEN, "You can't get that users transaction")
        }

        val page = PageRequest.of(pageNumber ?: 0, REQUEST_SIZE, Sort.by("transactionAt").descending())
        return transactionRepository.findAllByScannerUserId(scannerId, page)
    }

    fun getTransaction(id: String): Transaction {
        return transactionRepository.findById(id)
            .orElseThrow { TransactionException(id = id) }
    }

    fun getTransactionPendingByUserId(userId: String, pageNumber: Int?): Transaction {
        val page = PageRequest.of(pageNumber ?: 0, 100, Sort.by("transactionAt").descending())
        val transactions = transactionRepository.findAllByUserIdAndStatus(userId, PENDING, page)

        logger.info("Transaction size = ${transactions.size}")
        return getFirstPendingTransaction(transactions)
    }

    fun setTransactionStatus(userId: String, id: String, newStatus: TransactionStatus, pageNumber: Int?): Transaction {
        val page = PageRequest.of(pageNumber ?: 0, 100, Sort.by("transactionAt").descending())
        val transactions = transactionRepository.findAllByUserIdAndStatus(userId, PENDING, page)

        val transaction = getFirstPendingTransaction(transactions)
        val now = Instant.now()

        transaction.status = newStatus
        transaction.statusChangedAt = now
        when (newStatus) {
            ACCEPTED -> {
                val tokensAccepted = transaction.tokens.map {
                    it.status = TokenStatus.USED
                    it.lastChangeAt = now
                    it
                }
                tokenRepository.saveAll(tokensAccepted)
            }
            REJECTED -> {
                val tokensBackToUsable = transaction.tokens.map {
                    it.status = TokenStatus.USABLE
                    it.lastChangeAt = now
                    it
                }
                tokenRepository.saveAll(tokensBackToUsable)
            }
            else -> throw RuntimeException("PENDING should not happen here")
        }
        return transactionRepository.save(transaction)
    }

    //endpoint for scanner
    fun createTransaction(walletId: String, tokensToUse: Set<TransactionsResource.TokensToBuy>, scanningUserId: String): Transaction {
        val userId = walletService.getUserIdByWalletId(walletId)
            ?: return transactionRepository.save(Transaction.fakePendingTransaction())


        val page = PageRequest.of(0, 1000, Sort.by("transactionAt").descending())
        val pendingTransactions = transactionRepository.findAllByUserIdAndStatus(userId, PENDING, page)

        if (pendingTransactions.isNotEmpty()) {
            throw TooManyPendingTransaction()
        }

        val usersTokens = tokenRepository.findAllByOwnerId(userId)
            .filter { it.status == TokenStatus.USABLE }
            .groupBy { it -> it.buyableTokenId }

        val user = userRepository.findById(userId).orElseThrow { throw ApiException(FORBIDDEN, "Bad credentials") }
        val scanner = userRepository.findById(scanningUserId).orElseThrow { throw ApiException(FORBIDDEN, "Bad credentials") }

        if (usersTokensContainsAllTokensToUse(tokensToUse, usersTokens)) {
            val reservedTokens = reserveTokens(tokensToUse, usersTokens)
            val exampleReservedToken = reservedTokens.first()
            tokenRepository.saveAll(reservedTokens)
            return transactionRepository.save(
                Transaction(
                    id = UUID.randomUUID().toString(),
                    scannerUserId = scanningUserId,
                    userId = userId,
                    eventId = exampleReservedToken.eventId,
                    eventName = exampleReservedToken.eventName,
                    tokens = reservedTokens,
                    status = PENDING,
                    scannerUserName = scanner.username,
                    userName = user.username
                )
            )
        } else {
            //create fake transaction so user will get notification
            transactionRepository.save(Transaction.fakePendingNotEnoughTokensTransaction(userId))
            logger.info("Created fake transaction")
            throw NotEnoughTokensException()
        }
    }

    /*
    endpoint for scanner to reject pending(abandoned transaction)
     */
    fun rejectPendingTransaction(scannerId: String, id: String) {
        val transaction = transactionRepository.findById(id).orElseThrow { throw ApiException(NOT_FOUND, "No transaction to delete") }

        if (transaction.scannerUserId == scannerId && transaction.status == PENDING) {
            transaction.status = REJECTED
            transaction.statusChangedAt = Instant.now()
            val tokensBackToUsable = transaction.tokens.map {
                it.status = TokenStatus.USABLE
                it.lastChangeAt = Instant.now()
                it
            }
            tokenRepository.saveAll(tokensBackToUsable)
            transactionRepository.save(transaction)
            logger.info("Rejected transaction id: {} by scanner {}", transaction.id, scannerId)
        } else {
            logger.info("Unable to reject transaction. Different scannerId or transaction is not pending")
        }
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
        const val REQUEST_SIZE = 20
    }

    private fun getFirstPendingTransaction(transactions: List<Transaction>): Transaction {
        return when (transactions.size) {
            1 -> transactions.first()
            0 -> throw PendingTransactionNotFound()
            else -> throw TooManyPendingTransaction()
        }
    }

    private fun usersTokensContainsAllTokensToUse(tokensToUse: Set<TransactionsResource.TokensToBuy>, usersTokens: Map<String, List<Token>>) =
        tokensToUse.all { it.quantity <= usersTokens.getOrDefault(it.id, emptyList()).size }

    private fun reserveTokens(tokensToUse: Set<TransactionsResource.TokensToBuy>, usersTokens: Map<String, List<Token>>): List<Token> {
        return tokensToUse.flatMap {
            val id = it.id
            usersTokens
                .getOrDefault(id, emptyList())
                .take(it.quantity)
                .map {
                    it.status = TokenStatus.RESERVED
                    it.lastChangeAt = Instant.now()
                    it
                }
        }
    }

    @Scheduled(fixedRate = 120000)
    fun setTransactionsToRejected() {
        val transactions = transactionRepository.findAllByStatus(PENDING)
        val now = Instant.now()

        //todo filter all 'fake' transactions that were made only
        //todo because user had no enough tokens.
        transactions
            .filter { it.eventId == "ERROR_SCAN_NO_TOKENS" }
            .forEach { transactionRepository.delete(it) }

        val rejectedTransactions = transactions.filter { Duration.between(it.transactionAt, now).abs().toMinutes() > 1 }
            .map {
                it.status = REJECTED
                it.statusChangedAt = now
                it.tokens.forEach { token ->
                    token.status = TokenStatus.USABLE
                    token.lastChangeAt = now
                }
                it
            }
        transactionRepository.saveAll(rejectedTransactions)
    }

    fun delete(transaction: Transaction) {
        transactionRepository.delete(transaction)
    }

    fun findDetailsById(transactionId: String): TransactionDetails = TransactionDetails.of(transactionRepository
        .findById(transactionId).orElseThrow { throw ApiException(NOT_FOUND, "Transaction not found") })

    fun findAllByEventId(creatorId: String, eventId: String, page: Int?): List<Transaction> {
        val pageRequest = PageRequest.of(page ?: 0, 20)

        return transactionRepository.findAllByEventIdOrderByTransactionAtDesc(eventId, pageRequest)
    }
}
