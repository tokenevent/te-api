package pl.tokenevent.infra

import pl.tokenevent.domain.Transaction
import pl.tokenevent.domain.TransactionStatus
import pl.tokenevent.resources.eventmanager.EventSummary
import pl.tokenevent.resources.eventmanager.SingleTokenSummary
import pl.tokenevent.resources.eventmanager.TokensSummary
import pl.tokenevent.resources.eventmanager.TransactionsSummary

class DetailCreator {
    companion object {
        fun fromTransactions(transactions: List<Transaction>): EventSummary {
            val filteredTransactions = transactions.filter { it.status == TransactionStatus.ACCEPTED }
            val allTokens = filteredTransactions.flatMap { it.tokens }
            val totalAmount = allTokens
                .map { it.tokenValue }
                .reduce { acc, d -> d + acc }
            val distinctUsers = filteredTransactions.distinctBy { it.userName }

            val tokensSummary = allTokens.groupBy { it.buyableTokenName }
                .map { group ->
                    SingleTokenSummary(
                        group.key,
                        group.value.size,
                        group.value.sumBy { it.tokenValue.toInt() }.toLong()
                    )
                }

            return EventSummary(
                transactions = TransactionsSummary(
                    count = filteredTransactions.size,
                    totalAmount = totalAmount),
                uniqueUsers = distinctUsers.size,
                tokens = TokensSummary(
                    tokens = tokensSummary
                )
            )
        }
    }
}
