package pl.tokenevent.infra.buytokenhistory

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.stereotype.Service
import pl.tokenevent.domain.BuyTokenTransaction
import pl.tokenevent.domain.BuyTokenTransactionStatus
import pl.tokenevent.exceptions.ApiException
import pl.tokenevent.repositories.BuyTokenTransactionsRepository
import pl.tokenevent.repositories.EventRepository

@Service
class BuyTokenHistoryService(
    val buyTokenTransactionsRepository: BuyTokenTransactionsRepository,
    val eventRepository: EventRepository
) {
    fun findAll(): List<BuyTokenTransaction> = buyTokenTransactionsRepository.findAll()

    fun findAllByUserId(userId: String, pageNumber: Int?): List<BuyTokenTransaction> {
        val page = PageRequest.of(pageNumber ?: 0, 20, Sort.by("createdAt").descending())
        return buyTokenTransactionsRepository.findAllByUserId(userId, page)
    }

    fun findAllPurchasedByEventAndOwner(eventOwnerId: String, eventId: String, pageNumber: Int?): List<BuyTokenTransaction> {
        val event = eventRepository.findById(eventId).orElseThrow { throw ApiException(NOT_FOUND, "Event not found") }
        if (event.creatorId != eventOwnerId) {
            throw ApiException(FORBIDDEN, "You can't get that event history")
        }
        val page = PageRequest.of(pageNumber ?: 0, 20, Sort.by("createdAt").descending())
        return buyTokenTransactionsRepository.findAllByEventIdOrderByCreatedAtDesc(eventId, page)
    }

    fun findSingleTransactionUpdateStatus(id: String, userId: String): SingleTransactionUpdateStatus {
        val transaction = buyTokenTransactionsRepository.findById(id)

        return transaction
            .map {
                if (it.userId != userId) {
                    throw ApiException(FORBIDDEN, "You're not allowed to see $id transaction")
                }
                SingleTransactionUpdateStatus(
                    it.status,
                    it.tokens.size
                )
            }
            .orElseThrow { throw ApiException(FORBIDDEN, "You're not allowed to see $id transaction") }
    }

    fun findById(transactionId: String, userId: String): BuyTokenTransaction = buyTokenTransactionsRepository.findById(transactionId)
        .orElseThrow { throw ApiException(NOT_FOUND, "You can't get that event history") }
}

data class SingleTransactionUpdateStatus(
    val currentStatus: BuyTokenTransactionStatus,
    val tokenCount: Int?
)
