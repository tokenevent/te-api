package pl.tokenevent.infra.configuration.security;

import com.google.common.collect.ImmutableMap;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.compression.GzipCompressionCodec;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;
import java.util.function.Supplier;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static io.jsonwebtoken.impl.TextCodec.BASE64;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.removeStart;
import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

@Service
final class JWTTokenService implements Clock, TokenService {
    private static final String BEARER = "Bearer";
    private static final String DOT = ".";
    private static final GzipCompressionCodec COMPRESSION_CODEC = new GzipCompressionCodec();

    private String issuer;
    private int expirationSec;
    private int clockSkewSec;
    private String secretKey;

    JWTTokenService(
        @Value("${jwt.issuer:octoperf}") final String issuer,
        @Value("${jwt.expiration-sec:86400}") final int expirationSec,
        @Value("${jwt.clock-skew-sec:300}") final int clockSkewSec,
        @Value("${jwt.secret:secret}") final String secret) {
        super();
        this.issuer = requireNonNull(issuer);
        this.expirationSec = expirationSec;
        this.clockSkewSec = clockSkewSec;
        this.secretKey = BASE64.encode(requireNonNull(secret));
    }

    private static Map<String, String> parseClaims(final Supplier<Claims> toClaims) {
        try {
            final Claims claims = toClaims.get();
            final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
            for (final Map.Entry<String, Object> e : claims.entrySet()) {
                builder.put(e.getKey(), String.valueOf(e.getValue()));
            }
            return builder.build();
        } catch (final IllegalArgumentException | JwtException e) {
            return ImmutableMap.of();
        }
    }

    @NotNull
    public static String getTokenFromHeader(String param) {
        return ofNullable(param)
            .map(value -> removeStart(value, BEARER))
            .map(String::trim)
            .orElseThrow(() -> new BadCredentialsException("Missing Authentication Token"));
    }

    @Override
    public String permanent(final Map<String, String> attributes) {
        return newToken(attributes, 0);
    }

    @Override
    public String expiring(final Map<String, String> attributes) {
        return newToken(attributes, expirationSec);
    }

    @Override
    public Map<String, String> verify(final String token) {
        final JwtParser parser = Jwts
            .parser()
            .requireIssuer(issuer)
            .setClock(this)
            .setAllowedClockSkewSeconds(clockSkewSec)
            .setSigningKey(secretKey);
        return parseClaims(() -> parser.parseClaimsJws(token).getBody());
    }

    @Override
    public Map<String, String> untrusted(final String token) {
        final JwtParser parser = Jwts
            .parser()
            .requireIssuer(issuer)
            .setClock(this)
            .setAllowedClockSkewSeconds(clockSkewSec);

        // See: https://github.com/jwtk/jjwt/issues/135
        final String withoutSignature = substringBeforeLast(token, DOT) + DOT;
        return parseClaims(() -> parser.parseClaimsJwt(withoutSignature).getBody());
    }

    @Override
    public Date now() {
        final LocalDateTime ldt = getNowLocaleDate();
        return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }

    private String newToken(final Map<String, String> attributes, final int expiresInSec) {
        Date date = now();
        final Claims claims = Jwts
            .claims()
            .setIssuer(issuer)
            .setIssuedAt(date);

        if (expiresInSec > 0) {
            final LocalDateTime expiresAt = getNowLocaleDate().plus(expiresInSec, ChronoUnit.SECONDS);
            Date expiresAtDate = Date.from(expiresAt.atZone(ZoneId.systemDefault()).toInstant());
            claims.setExpiration(expiresAtDate);
        }
        claims.putAll(attributes);

        return Jwts
            .builder()
            .setClaims(claims)
            .signWith(HS256, secretKey)
            .compressWith(COMPRESSION_CODEC)
            .compact();
    }

    @NotNull
    private LocalDateTime getNowLocaleDate() {
        return LocalDateTime.now();
    }
}
