package pl.tokenevent.infra.configuration.security;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.tokenevent.infra.users.User;
import pl.tokenevent.infra.users.UserAuthenticationService;
import pl.tokenevent.infra.users.UserService;

import java.util.Optional;

import static java.util.Optional.of;

@Service
final class TokenAuthenticationService implements UserAuthenticationService {
    private TokenService tokens;
    private UserService users;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public TokenAuthenticationService(TokenService tokens, UserService users, PasswordEncoder passwordEncoder) {
        this.tokens = tokens;
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<String> login(final String username, final String password) {
        return users
            .findByUsername(username)
            .filter(user -> passwordEncoder.matches(password, user.getPassword()))
            .map(user -> tokens.expiring(ImmutableMap.of("username", username)));
    }

    @Override
    public Optional<User> findByToken(final String token) {
        return of(tokens.verify(token))
            .map(map -> map.get("username"))
            .flatMap(users::findByUsername);
    }

    @Override
    public void logout(final User user) {

    }
}