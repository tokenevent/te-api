package pl.tokenevent.infra.eventmanager

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.stereotype.Service
import pl.tokenevent.exceptions.EventManagerException
import pl.tokenevent.infra.users.User
import pl.tokenevent.infra.users.User.scannerUser
import pl.tokenevent.infra.users.UserRepository
import pl.tokenevent.repositories.EventRepository
import java.lang.invoke.MethodHandles

@Service
class EventManagerFacade(
    val userRepository: UserRepository,
    val eventRepository: EventRepository
) {
    fun createNewScanner(creatorId: String, eventId: String, newUsername: String, newPassword: String): User {
        validateEventOwnership(eventId, creatorId)

        logger.info("Creating new user($newUsername) for event: $eventId by $creatorId.")
        return userRepository.save(scannerUser(
            newUsername,
            newPassword,
            eventId,
            creatorId
        ))
    }

    fun deleteScanner(parentId: String, eventId: String, scannerId: String) {
        val toDeleteUser = userRepository.findById(scannerId).orElseThrow { throw EventManagerException(NOT_FOUND, "ScannerId: $scannerId") }

        if (toDeleteUser.parentId != parentId) {
            throw EventManagerException(FORBIDDEN, "You can't delete that scanner")
        }

        userRepository.delete(toDeleteUser)
    }

    fun getScannersByCreatorId(creatorId: String, eventId: String): List<User> {
        logger.info("Getting scanners from event $eventId creatorId $creatorId")
        return userRepository.findAllByParentIdAndEventId(creatorId, eventId)
    }

    private fun validateEventOwnership(eventId: String, creatorId: String) {
        val event = eventRepository.findById(eventId)
            .orElseThrow { throw EventManagerException(NOT_FOUND, "Cannot create user. EventId: $eventId does not exist") }
        if (event.creatorId != creatorId) {
            throw EventManagerException(FORBIDDEN, "User $creatorId cannot modify $eventId")
        }
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }
}
