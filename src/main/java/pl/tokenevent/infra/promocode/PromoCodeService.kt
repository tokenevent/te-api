package pl.tokenevent.infra.promocode

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import pl.tokenevent.domain.BuyTokenTransaction
import pl.tokenevent.domain.Promotion
import pl.tokenevent.domain.PromotionType
import pl.tokenevent.exceptions.ApiException
import pl.tokenevent.infra.tokensIssue.TokenIssuerService
import pl.tokenevent.repositories.BuyTokenTransactionsRepository
import pl.tokenevent.repositories.PromoCodeRepository
import java.lang.invoke.MethodHandles

@Service
class PromoCodeService(
    private val promoCodeRepository: PromoCodeRepository,
    private val issuerService: TokenIssuerService
) {
    fun isValidCode(code: String?): Boolean {
        if (code == null) {
            return false
        }
        val promotion = promoCodeRepository.findAllByCode(code) ?: return false
        return promotion.isActive
    }

    fun usePromoCode(userId: String, promoCode: String) {
        val promotion = promoCodeRepository.findAllByCode(promoCode)
            ?: throw ApiException(HttpStatus.BAD_REQUEST, "Promo code does not exist or is inactive.")

        if (promotion.isActive.not()) throw ApiException(HttpStatus.BAD_REQUEST, "Promo code does not exist or is inactive.")

        issuerService.issueTokensWithPromoCode(
            userId = userId,
            eventId = promotion.eventId,
            promoType = promotion.type
        )
    }

    fun addPromoCode(code: String, eventId: String): Promotion {
        if (promoCodeRepository.findAllByCode(code) != null) {
            throw ApiException(HttpStatus.BAD_REQUEST, "$code promo code exists. Use another code")
        }
        logger.info("Adding {} promo code for event {}", code, eventId)
        return promoCodeRepository.save(
            Promotion(code, PromotionType.ONE_OF_EACH, eventId, true)
        )
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }
}
