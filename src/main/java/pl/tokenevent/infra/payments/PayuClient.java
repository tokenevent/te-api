package pl.tokenevent.infra.payments;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import pl.tokenevent.infra.payments.dto.PayuAuthRequest;
import pl.tokenevent.infra.payments.dto.PayuAuthResponse;
import pl.tokenevent.infra.payments.dto.PayuNewOrderRequest;
import pl.tokenevent.infra.payments.dto.PayuNewOrderResponse;

import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.util.Objects;

@Component
public class PayuClient {
    private final String payuSandboxUrl = "https://secure.snd.payu.com";
    private final String payuProdUrl = "https://secure.payu.com";
    private final String payuOauthUrl = "/pl/standard/user/oauth/authorize";
    private final String payuOrderUrl = "/api/v2_1/orders";
    private final String grantType = "client_credentials";
    private final String clientId = "369617";
    private final String clientSecret = "59a8230d7792ade8a88e45ade5ccfd70";

    private String accessToken = null;
    private Instant expiresAt = Instant.MAX;

    private Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private RestTemplate restTemplate;

    public PayuClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getAccessToken() {
        if (accessToken != null && isNotExpired()) {
            return accessToken;
        }
        logger.info("Getting new access token");
        PayuAuthRequest payuAuthRequest = new PayuAuthRequest(grantType, clientId, clientSecret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(payuAuthRequest.toMap(), headers);

        try {
            ResponseEntity<PayuAuthResponse> response = restTemplate
                .exchange(payuSandboxUrl + payuOauthUrl, HttpMethod.POST, request, PayuAuthResponse.class);
            accessToken = Objects.requireNonNull(response.getBody()).getAccessToken();

            int expiresIn = response.getBody().getExpiresIn();
            expiresAt = Instant.now().plusSeconds(expiresIn);

            return accessToken;
        } catch (Exception e) {
            logger.info(e.getMessage());
            throw new RuntimeException();
        }
    }

    public PayuNewOrderResponse newOrder(@NotNull PayuNewOrderRequest order) {
        HttpEntity<PayuNewOrderRequest> request = new HttpEntity<>(order, getHeaders());

        try {
            ResponseEntity<PayuNewOrderResponse> response = restTemplate
                .exchange(payuSandboxUrl + payuOrderUrl, HttpMethod.POST, request, PayuNewOrderResponse.class);
            return response.getBody();
        } catch (Exception e) {
            logger.info(e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    @NotNull
    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getAccessToken());
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    private boolean isNotExpired() {
        return expiresAt.isAfter(Instant.now());
    }
}
