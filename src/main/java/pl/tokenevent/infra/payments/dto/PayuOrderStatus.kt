package pl.tokenevent.infra.payments.dto

import pl.tokenevent.domain.BuyTokenTransactionStatus

data class PayuOrderStatus(
    val statusCode: BuyTokenTransactionStatus
)
