package pl.tokenevent.infra.payments.dto

import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

data class PayuAuthRequest(
    val grantType: String,
    val clientId: String,
    val clientSecret: String
) {
    fun toMap(): MultiValueMap<String, String> {
        val map = LinkedMultiValueMap<String, String>()
        map.add("grant_type", this.grantType)
        map.add("client_id", this.clientId)
        map.add("client_secret", this.clientSecret)
        return map
    }
}


