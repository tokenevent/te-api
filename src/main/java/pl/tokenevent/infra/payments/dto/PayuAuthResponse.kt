package pl.tokenevent.infra.payments.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class PayuAuthResponse(
    @JsonProperty("access_token")
    val accessToken: String,
    @JsonProperty("expires_in")
    val expiresIn: Int
)
