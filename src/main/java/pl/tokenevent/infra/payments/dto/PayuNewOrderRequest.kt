package pl.tokenevent.infra.payments.dto

data class PayuNewOrderRequest(
    val notifyUrl: String,
    val customerIp: String,
    val merchantPosId: Int = 369617,
    val description: String = "Płatność za żetony",
    val currencyCode: String = "PLN",
    val totalAmount: Long,
    val extOrderId: String,
    val buyer: PayuBuyer,
    val settings: PayuSettings,
    val products: List<PayuProduct>
)
