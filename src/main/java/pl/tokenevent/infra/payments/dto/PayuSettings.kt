package pl.tokenevent.infra.payments.dto

data class PayuSettings(
    val invoiceDisabled: Boolean = false
)
