package pl.tokenevent.infra.payments.dto

data class PayuProduct(
    val name: String,
    val unitPrice: Long,
    val quantity: Int
)
