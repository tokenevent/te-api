package pl.tokenevent.infra.payments.dto

data class PayuNotificationProperties(
    val name: String,
    val value: String
)
