package pl.tokenevent.infra.payments.dto

data class PayuNewOrderResponse(
    val status: PayuOrderStatusResponse,
    val redirectUri: String,
    val orderId: String,
    val extOrderId: String
)

data class PayuOrderStatusResponse(
    val statusCode: String
)
