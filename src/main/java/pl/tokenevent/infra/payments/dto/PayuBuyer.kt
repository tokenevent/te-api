package pl.tokenevent.infra.payments.dto

data class PayuBuyer(
    val email: String = "kteske@tokenevent.pl",
    val phone: String = "505988112",
    val firstName: String = "karol",
    val lastName: String = "tesk",
    val language: String = "pl"
)
