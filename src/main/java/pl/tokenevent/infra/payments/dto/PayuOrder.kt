package pl.tokenevent.infra.payments.dto

data class PayuOrder(
    val orderId: String?,
    val extOrderId: String,
    val orderCreateDate: String?,
    val notifyUrl: String?,
    val customerIp: String?,
    val merchantPosId: String?,
    val description: String?,
    val currencyCode: String?,
    val totalAmount: String?,
    val buyer: PayuBuyer?,
    val payMethod: PayuPayMethod?,
    val products: List<PayuProduct>,
    val status: PayuStatus
)

enum class PayuStatus {
    PENDING, WAITING_FOR_CONFIRMATION, COMPLETED, CANCELED
}
