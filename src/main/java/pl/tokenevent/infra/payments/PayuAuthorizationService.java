package pl.tokenevent.infra.payments;

import org.springframework.stereotype.Service;

@Service
public class PayuAuthorizationService implements PaymentAuthorizationService {
    private PayuClient payuClient;

    public PayuAuthorizationService(PayuClient payuClient) {
        this.payuClient = payuClient;
    }

    public String getAccessToken() {
        return payuClient.getAccessToken();
    }
}
