package pl.tokenevent.infra.payments;

public interface PaymentAuthorizationService {
    String getAccessToken();
}
