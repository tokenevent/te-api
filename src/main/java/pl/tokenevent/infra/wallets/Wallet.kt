package pl.tokenevent.infra.wallets

import java.time.Instant

data class Wallet(
    val id: String,
    val userId: String,
    val createdAt: Instant
)
