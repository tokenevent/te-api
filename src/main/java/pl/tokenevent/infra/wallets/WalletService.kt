package pl.tokenevent.infra.wallets

import com.google.common.cache.CacheBuilder
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.concurrent.TimeUnit

@Service
class WalletService {
    companion object {
        const val VALID_FOR: Long = 61
    }

    private val wallets = CacheBuilder.newBuilder()
        .maximumSize(10000000)
        .expireAfterWrite(VALID_FOR, TimeUnit.SECONDS)
        .build<String, Wallet>() //always userId and walletId

    fun getWalletIdByUserId(userId: String): Wallet {
        return wallets.getIfPresent(userId) ?: createNewWallet(userId)
    }

    fun getUserIdByWalletId(walletId: String): String? {
        return wallets.getIfPresent(walletId)?.userId
    }

    private fun createNewWallet(userId: String): Wallet {
        val newWalletId = RandomStringUtils.randomNumeric(6)
        val now = Instant.now()

        val wallet = Wallet(id = newWalletId, userId = userId, createdAt = now)
        wallets.put(userId, wallet)
        wallets.put(newWalletId, wallet)
        return wallet
    }
}


