package pl.tokenevent.infra.users

data class Account(
    val email: String,
    val phone: String,
    val firstName: String,
    val lastName: String
)
