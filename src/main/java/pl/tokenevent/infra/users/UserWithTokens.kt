package pl.tokenevent.infra.users

import pl.tokenevent.resources.dto.EventWithTokens

data class UserWithTokens(
    val user: User,
    val events: List<EventWithTokens>
)
