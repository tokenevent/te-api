package pl.tokenevent.infra.users

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.stereotype.Service
import pl.tokenevent.domain.TokenStatus
import pl.tokenevent.exceptions.ApiException
import pl.tokenevent.exceptions.CredentialsException
import pl.tokenevent.exceptions.UsernameTakenException
import pl.tokenevent.infra.wallets.Wallet
import pl.tokenevent.infra.wallets.WalletService
import pl.tokenevent.repositories.EventRepository
import pl.tokenevent.repositories.TokenRepository
import pl.tokenevent.resources.EditSettingsRequest
import pl.tokenevent.resources.dto.EventWithTokens
import java.lang.invoke.MethodHandles
import java.util.Optional

@Service
class UserService(
    private val userRepository: UserRepository,
    private val tokenRepository: TokenRepository,
    private val eventRepository: EventRepository,
    private val walletService: WalletService
) {
    fun saveUser(user: User): User {
        if (findByUsername(user.username).isPresent) {
            throw UsernameTakenException()
        }

        return userRepository.save(user)
    }

    fun getUser(id: String): Optional<User> {
        return userRepository.findById(id)
    }

    fun findByUsername(username: String): Optional<User> {
        return userRepository.findByUsername(username)
    }

    fun getUserWithTokens(userId: String): UserWithTokens {
        val tokens = tokenRepository.findAllByOwnerId(userId)
        val user = userRepository.findById(userId)
        val userFromDb = user.orElseThrow { CredentialsException() }

        val events = tokens.map { it.eventId }
            .distinct()
            .map { eventRepository.findById(it).get() }
            .map {
                val tokensForThatEvent = tokens.filter { token -> token.eventId == it.id && token.status == TokenStatus.USABLE }
                EventWithTokens(it, tokensForThatEvent)
            }
        return UserWithTokens(userFromDb, events)
    }

    fun getWalletByUserId(userId: String): Wallet {
        return walletService.getWalletIdByUserId(userId)
    }

    fun setSettings(userId: String, request: EditSettingsRequest): User {
        val user = userRepository.findById(userId).orElseThrow { throw ApiException(NOT_FOUND, "That user does not exist") }

        val email = request.email?.takeIfNotEmptyAndLessThan100() ?: user.account.email
        val phone = request.phone?.takeIfNotEmptyAndLessThan100() ?: user.account.phone
        val firstName = request.firstName?.takeIfNotEmptyAndLessThan100() ?: user.account.firstName
        val lastName = request.lastName?.takeIfNotEmptyAndLessThan100() ?: user.account.lastName

        user.account = Account(email, phone, firstName, lastName)
        logger.info("User id: {}, name: {}, changed his account to {}", user.id, user.username, user.account)
        return userRepository.save(user)
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }

    fun String.takeIfNotEmptyAndLessThan100(): String? {
        if (this.isNotEmpty() && this.length <= 100){
            return this
        }
        throw ApiException(BAD_REQUEST, "'$this' should not be empty and less than 100 chars.")
    }
}
