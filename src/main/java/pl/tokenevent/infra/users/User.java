package pl.tokenevent.infra.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.tokenevent.resources.dto.LoginFormDTO;
import pl.tokenevent.resources.dto.UserRegisterRequest;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.requireNonNull;
import static pl.tokenevent.infra.Roles.ROLE_SCANNER;

public class User implements UserDetails {
    private static final long serialVersionUID = 2396654715019746670L;
    private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Indexed
    @Id
    private String id;
    @Indexed
    private String username;
    private String password;
    private List<GrantedAuthority> authorities;
    private Instant createdAt;
    private boolean enabled;
    @Indexed
    private String parentId;
    @Indexed
    private String eventId;
    private Account account;

    public User() {
    }

    @JsonCreator
    public User(@JsonProperty("id") final String id,
                @JsonProperty("username") final String username,
                @JsonProperty("password") final String password,
                List<GrantedAuthority> authorities,
                Account account) {
        super();
        this.id = requireNonNull(id);
        this.username = requireNonNull(username);
        this.password = requireNonNull(password);
        this.authorities = authorities;
        createdAt = Instant.now();
        enabled = true;
        this.account = account;
    }

    //do rejestracji adminow
    @JsonCreator
    public User(@JsonProperty("id") final String id,
                @JsonProperty("username") final String username,
                @JsonProperty("password") final String password,
                List<GrantedAuthority> authorities
    ) {
        super();
        this.id = requireNonNull(id);
        this.username = requireNonNull(username);
        this.password = requireNonNull(password);
        this.authorities = authorities;
        createdAt = Instant.now();
        enabled = true;
        this.account = null;
    }

    //dla skannera
    public User(
        String username,
        String password,
        String parentId,
        String eventId
    ) {
        this(UUID.randomUUID().toString(),
            username,
            encoder.encode(password),
            Lists.newArrayList(new SimpleGrantedAuthority(ROLE_SCANNER.toString())),
            null
        );
        this.parentId = parentId;
        this.eventId = eventId;
    }

    public static User of(UserRegisterRequest request, List<GrantedAuthority> authorities) {
        return new User(UUID.randomUUID().toString(),
            request.getUsername(),
            encoder.encode(request.getPassword()),
            authorities,
            new Account(request.getEmail(), request.getPhone(), request.getFirstName(), request.getLastName())
        );
    }

    public static User adminOf(LoginFormDTO request, List<GrantedAuthority> authorities) {
        return new User(UUID.randomUUID().toString(),
            request.getUsername(),
            encoder.encode(request.getPassword()),
            authorities
        );
    }

    public static User scannerUser(String username, String password, String eventId, String parentId) {
        return new User(username, password, parentId, eventId);
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    public BCryptPasswordEncoder getEncoder() {
        return encoder;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public String getId() {
        return id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public String getParentId() {
        return parentId;
    }

    public String getEventId() {
        return eventId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
