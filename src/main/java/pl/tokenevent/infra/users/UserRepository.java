package pl.tokenevent.infra.users;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByUsername(String username);

    Optional<User> findByParentId(String parentId);

    List<User> findAllByParentId(String parentId);

    List<User> findAllByParentIdAndEventId(String parentId, String EventId);
}

