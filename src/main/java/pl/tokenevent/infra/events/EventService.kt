package pl.tokenevent.infra.events

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import pl.tokenevent.domain.BuyableToken
import pl.tokenevent.domain.Event
import pl.tokenevent.exceptions.AccessException
import pl.tokenevent.exceptions.ApiException
import pl.tokenevent.exceptions.EventManagerException
import pl.tokenevent.exceptions.ResourceNotFoundException
import pl.tokenevent.infra.DetailCreator
import pl.tokenevent.infra.users.UserRepository
import pl.tokenevent.repositories.EventRepository
import pl.tokenevent.repositories.TokenRepository
import pl.tokenevent.repositories.TransactionRepository
import pl.tokenevent.resources.dto.UpdateEventRequest
import pl.tokenevent.resources.eventmanager.EventSummary
import java.lang.invoke.MethodHandles
import java.util.Optional

@Service
class EventService(
    private val eventRepository: EventRepository,
    private val tokenRepository: TokenRepository,
    private val coordinatesService: CoordinatesService,
    private val transactionRepository: TransactionRepository,
    private val userRepository: UserRepository
) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }

    fun getEvent(id: String): Optional<Event> {
        return eventRepository.findById(id)
    }

    fun save(event: Event): Event {
        val newEvent = setCoordinates(event)

        return eventRepository.save(newEvent)
    }

    fun update(updateEventRequest: UpdateEventRequest, requestingUserId: String, eventId: String): Event {
        val event = eventRepository.findById(eventId).orElseThrow { AccessException("Event does not exist") }
        event.also { validateUserCreator(it.creatorId, requestingUserId) }

        updateEventRequest.city?.let {
            event.address.city = it
        }

        updateEventRequest.description?.let {
            event.description = it
        }
        updateEventRequest.startingAt?.let {
            event.startingAt = it
        }
        updateEventRequest.endingAt?.let {
            event.endingAt = it
        }
        updateEventRequest.buyableTokens?.let { it ->
            event.buyableTokens = it.map { new -> new.toBuyableToken() }.toSet()
        }
        updateEventRequest.city?.let {
            event.address.city = it
        }
        updateEventRequest.street?.let {
            event.address.street = it
        }
        updateEventRequest.zipCode?.let {
            event.address.zipCode = it
        }

        logger.info("Updated event: {}, with: {}", event.id, updateEventRequest)
        return eventRepository.save(event)
    }

    fun findAll(): Iterable<Event> {
        return eventRepository.findAll()
    }

    fun findEventsByCreatorId(creatorId: String): List<Event> {
        return eventRepository.findEventsByCreatorId(creatorId)
    }

    fun delete(id: String) {
        val event: Event = eventRepository
            .findById(id)
            .orElseThrow { ResourceNotFoundException() }
        eventRepository.delete(event)
    }

    fun deleteBuyableToken(requestingUserId: String, id: String, name: String) {
        val event = eventRepository.findById(id).orElseThrow { ResourceNotFoundException() }
        validateUserCreator(event.creatorId, requestingUserId)

        val tokenToDelete = event.buyableTokens.filter { it.name == name }.getOrNull(0)

        //validate if we have issued any token for that buyable token
        if (tokenToDelete != null) {
            val anyToken = tokenRepository.findFirstByBuyableTokenName(tokenToDelete.name)
            if (anyToken != null) {
                throw ApiException(HttpStatus.FORBIDDEN, "You can't delete buyable token if it has existing tokens")
            }
        }

        val filteredBuyableTokens = event.buyableTokens.filter { it.name != name }.toSet()
        event.buyableTokens = filteredBuyableTokens

        logger.info("User $requestingUserId deleted buyable token with name $name")
        eventRepository.save(event)
    }

    fun addBuyableToken(buyableTokens: Set<BuyableToken>, eventId: String, requestingUserId: String): Event {
        val event = eventRepository.findById(eventId).orElseThrow { ResourceNotFoundException() }

        validateUserCreator(event.creatorId, requestingUserId)

        val newBuyableTokens = event.buyableTokens.union(buyableTokens)
        event.buyableTokens = newBuyableTokens

        return eventRepository.save(event)
    }

    private fun validateUserCreator(creatorId: String, requestingUserId: String) {
        if (creatorId != requestingUserId) {
            throw AccessException("CreatorID and requestingUserId mismatch.")
        }
    }

    private fun setCoordinates(event: Event): Event {
        coordinatesService.getCoords(event.address)?.let {
            event.address.coordinates = it
        }
        return event
    }

    fun getEventSummary(eventId: String, requestingUserId: String): EventSummary {
        val event = eventRepository.findById(eventId).orElseThrow { throw ApiException(HttpStatus.NOT_FOUND, "Event not found") }
        if (event.creatorId != requestingUserId) {
            throw ApiException(HttpStatus.FORBIDDEN, "You can't get that event summary")
        }
        return DetailCreator.fromTransactions(transactionRepository.findAllByEventId(eventId))
    }

    fun getScannerSummary(scannerId: String, parentId: String): EventSummary {
        val scanner = userRepository.findById(scannerId).orElseThrow { throw EventManagerException(HttpStatus.NOT_FOUND, "ScannerId: $scannerId") }

        if (scanner.parentId != parentId) {
            throw EventManagerException(HttpStatus.FORBIDDEN, "You can't delete that scanner")
        }

        return DetailCreator.fromTransactions(transactionRepository.findAllByScannerUserId(scannerId))
    }
}
