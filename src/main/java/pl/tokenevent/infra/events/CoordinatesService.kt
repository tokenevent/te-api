package pl.tokenevent.infra.events

import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpStatusCodeException
import org.springframework.web.client.RestTemplate
import pl.tokenevent.domain.Coordinates
import pl.tokenevent.domain.EventAddress
import pl.tokenevent.exceptions.ApiException
import java.lang.invoke.MethodHandles

@Service
class CoordinatesService(
    private val restTemplate: RestTemplate,
    private val environment: Environment
) {
    companion object {
        val logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }

    fun getCoords(address: EventAddress): Coordinates? {
        val entity = HttpEntity.EMPTY

        //zeby nie strzelac do nich przy testach
        if (environment.activeProfiles.contains("integration")) {
            return Coordinates("1", "2")
        }

        return try {
            val start = System.currentTimeMillis()
            val response = restTemplate.exchange(
                //todo wywalic to do konfiguracji albo do beana
                "https://nominatim.openstreetmap.org/search?format=json&city=${address.city}&street=${address.street}&country=POLAND&postalcode=${address.zipCode}",
                HttpMethod.POST,
                entity,
                Array<CoordinatesResponse>::class.java,
                address
            )
            val stop = System.currentTimeMillis()
            val time = stop - start
            logger.info("Request for coords lasted: $time ms. Response: ${response.body}")
            if (response.body.size == 0) {
                throw ApiException(HttpStatus.CONFLICT, "Unable to process data. Add more specific location")
            }
            val coordinatesResponse = response.body[0]
            val data = coordinatesResponse
            Coordinates(data.lat, data.lon)
        } catch (exception: HttpStatusCodeException) {
            logger.info("Nominatim answered with: " + exception.statusCode + ". Exception: " + exception)
            null
        }
    }
}
