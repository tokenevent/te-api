package pl.tokenevent.infra.events

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class CoordinatesResponse(
    val lat: String,
    val lon: String
)
