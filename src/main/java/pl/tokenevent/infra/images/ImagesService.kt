package pl.tokenevent.infra.images

import org.springframework.stereotype.Service
import pl.tokenevent.domain.Image
import pl.tokenevent.exceptions.AccessException
import pl.tokenevent.exceptions.ResourceNotFoundException
import pl.tokenevent.repositories.ImagesRepository

@Service
class ImagesService(
    val imagesRepository: ImagesRepository
) {
    fun save(imageBase64Value: String?, creatorUserId: String): Image {
        if (imageBase64Value == null) {
            throw RuntimeException("Image base 64 can't be null")
        }
        return imagesRepository.save(Image(imageBase64Value, creatorUserId))
    }

    fun get(id: String): Image {
        return imagesRepository.findById(id).orElseThrow { ResourceNotFoundException() }
    }

    fun delete(id: String, userIdFrom: String) {

        val image = imagesRepository.findById(id).orElseThrow { ResourceNotFoundException() }
        if (image.creatorUserId != userIdFrom) {
            throw AccessException("You have no rights to delete that image.")
        }

        imagesRepository.delete(image)
    }

}
