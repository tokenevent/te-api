import groovyx.net.http.HttpResponseException
import org.springframework.beans.factory.annotation.Autowired
import pl.tokenevent.domain.BuyableToken
import pl.tokenevent.repositories.EventRepository

import static builders.EventBuilder.event
import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.SC_FORBIDDEN

class EventsSpec extends IntegrationSpec {

    @Autowired
    EventRepository eventRepository

    def "should get all events"() {
        given:
        def event1 = createEvent()
        def event2 = createEvent()

        when:
        def response = restClient.get path: "/events",
            headers: getHeaders()

        then:
        response.status == 200
        response.data.size == 2
        response.data.count { it.id == event1.id } == 1
        response.data.count { it.id == event2.id } == 1
    }

    def "should get specified event"() {
        given:
        def event = event()
            .withName("TestEvent")
            .withCreatorId("123-123")
            .withBuyableTokens([new BuyableToken("id1", "Czerwony", 200L),
                                new BuyableToken("id2", "Niebieski", 500)] as Set)
            .build()
        def savedEvent = eventRepository.save(event)

        when:
        def response = restClient.get path: "/events/${savedEvent.id}",
            headers: getHeaders(),
            requestContentType: JSON

        then:
        with(response.data) {
            id == event.id
            name == event.name
            creatorId != null
            buyableTokens.size == 2
            type.toString() == "VALUE"
        }
    }

    def "should create new event"() {
        given:
        def exampleName = "event-123"
        def request = createExampleEventRequest(exampleName)

        when:
        def response = restClient.post path: "/events-manager/events",
            body: request,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        response.data.name == exampleName
        response.data.address.city == "city"
        response.data.buyableTokens == []
    }

    def "should create new event and add two buyable tokens to it, 5 and 10 zl"() {
        given:
        def event = createEvent()
        def buyableToken5 = createBuyableTokenRequest(500)
        def buyableToken10 = createBuyableTokenRequest(1000)

        when:
        def response5 = restClient.put path: "/events-manager/events/$event.id/buyable-tokens",
            body: buyableToken5,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        response5.data.id == event.id
        response5.data.buyableTokens.size == 1
        response5.data.buyableTokens[0].id != null
        response5.data.buyableTokens[0].id != null
        response5.data.buyableTokens[0].name == "Token za 500"
        response5.data.buyableTokens[0].unitPrice == 500

        def response10 = restClient.put path: "/events-manager/events/$event.id/buyable-tokens",
            body: buyableToken10,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        response10.data.id == event.id
        response10.data.buyableTokens.size == 2
    }

    def "should create event and add two buyable tokens with single request"() {
        given:
        def event = createEvent()
        def request = createTwoBuyableTokenRequest(100, 200)

        when:
        def response = restClient.put path: "/events-manager/events/$event.id/buyable-tokens",
            body: request,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        response.data.id == event.id
        response.data.buyableTokens.size == 2
        response.data.buyableTokens[0].id != response.data.buyableTokens[1].id
    }

    def "should not be able to add buyable tokens to event that is created by someone else"() {
        given:
        def event = eventRepository.save(event().withCreatorId("any").build())
        def request = createBuyableTokenRequest(100)

        when:
        restClient.put path: "/events-manager/events/${event.id}/buyable-tokens",
            body: request,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == SC_FORBIDDEN
        e.response.data.status == "FORBIDDEN"
        e.response.data.message == "You're not allowed to do this. CreatorID and requestingUserId mismatch."
    }

    def "should create event with lat-long from nominatim"() {
        given:
        def exampleName = "event-123"
        def request = createExampleEventRequest(exampleName, "Poznań", "Śliska 19c")

        when:
        def response = restClient.post path: "/events-manager/events",
            body: request,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        response.data.name == exampleName
        response.data.address.city == "Poznań"
        response.data.address.street == "Śliska 19c"
        response.data.buyableTokens == []
        response.data.address.coordinates != null
    }

    def "should update event"() {
        given:
        String userId = getUserId(validEventManagerToken)
        def oldEvent = eventRepository.save(event().withCreatorId(userId).build())

        when:
        def response = restClient.put path: "/events-manager/events/${oldEvent.id}",
            body: [
                description: "New description",
                startingAt : "2019-11-19T08:00:00Z",
                city       : "Elblag"
            ],
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        with(response.data) {
            description == "New description"
            address.city == "Elblag"
            startingAt == "2019-11-19T08:00:00Z"
        }
    }
}
