import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.SC_CREATED

class PayuRedirectSpec extends IntegrationSpec {

    def "should return created with redirect to payu"() {
        given:
        def event = createEventWithTwoBuyableTokens()
        def issueTokenRequest = createExampleTokenIssueRequest(
            event.id,
            [[id: event.buyableTokens[0].id, quantity: 2]]
        )

        when:
        def response = restClient.post path: "/tokens/payu",
            body: issueTokenRequest,
            headers: getHeaders(validUserToken),
            requestContentType: JSON

        then:
        response.status == SC_CREATED
        response.getHeaders().getAt("Location").getValue() != null
    }
}
