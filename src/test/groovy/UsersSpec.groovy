import org.springframework.beans.factory.annotation.Autowired
import pl.tokenevent.domain.TokenStatus
import pl.tokenevent.repositories.TokenRepository

class UsersSpec extends IntegrationSpec {

    @Autowired
    TokenRepository tokenRepository

    def "should register user"() {
        given:
        def username = "user-1"
        def firstName = "Marian"
        def lastName = "Kowalski"
        def email = "kowal@test.pl"
        def phone = "505988111"
        def userCredentialsRequest = createUserRegisterRequest(username, "password123Q", email, phone, firstName, lastName)
        when:
        def user = registerUser(userCredentialsRequest)

        then:
        user.id != null
        user.username == username
        user.password == null
        user.createdAt != null
        with(user.account) {
            firstName == firstName
            lastName == lastName
            email == email
            phone == phone
        }
    }

    def "should be able to get users tokens as admin"() {
        given:
        def username = "user_123"
        def password = "haslo_123456"
        def userCredentialsRequest = createUserCredentialsRequest(username, password)
        def user = registerUser(userCredentialsRequest)
        def userToken = loginUser(userCredentialsRequest)

        def event = createEventWithTwoBuyableTokens()
        def tokens = buyFiveTokens(event, userToken)

        when:
        def response = restClient.get path: "/users/$user.id",
            headers: getHeaders(validAdminToken)

        then:
        response.status == 200
        response.data.user.username
        response.data.events.size == 1
        response.data.events.first().tokens.size == tokens.size
    }

    def "should be able to get my tokens"() {
        given:
        def username = "user_123"
        def password = "haslo_123456"
        def userCredentialsRequest = createUserCredentialsRequest(username, password)
        def user = registerUser(userCredentialsRequest)
        def userToken = loginUser(userCredentialsRequest)

        def event = createEventWithTwoBuyableTokens()
        def tokens = buyFiveTokens(event, userToken)

        when:
        def response = restClient.get path: "/users/me",
            headers: getHeaders(userToken)

        then:
        response.status == 200
        response.data.user.username
        response.data.events.size == 1
        response.data.events.first().tokens.size == tokens.size
    }

    def "should return only not used tokens"() {
        given:
        def username = "user_123"
        def password = "haslo_123456"
        def userCredentialsRequest = createUserCredentialsRequest(username, password)
        registerUser(userCredentialsRequest)
        def userToken = loginUser(userCredentialsRequest)

        def event = createEventWithTwoBuyableTokens()
        def tokens = buyFiveTokens(event, userToken)
        def newUsedToken = tokenRepository.findById(tokens[0].id).get()

        newUsedToken.status = TokenStatus.USED
        tokenRepository.save(newUsedToken)

        when:
        def response = restClient.get path: "/users/me",
            headers: getHeaders(userToken)

        then: "should return 4 tokens as one is marked as used"
        response.status == 200
        response.data.user.username
        response.data.events.size == 1
        response.data.events.first().tokens.size == 4
    }

    def "should return user wallet"() {
        when:
        def response = restClient.get path: "/users/me/wallet",
            headers: getHeaders(validAdminToken)

        then:
        response.status == 200
        response.data.id != null
    }

}
