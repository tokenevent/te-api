package builders

import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import pl.tokenevent.domain.BuyableToken
import pl.tokenevent.domain.Event
import pl.tokenevent.domain.EventAddress
import pl.tokenevent.domain.EventType

import java.time.Instant

@Builder(prefix = "with", builderStrategy = SimpleStrategy)
class EventBuilder {
    String id = UUID.randomUUID().toString()
    EventType type = EventType.VALUE
    String name = "event-name"
    String description = "some-desc"
    Instant startingAt = Instant.now()
    Instant endingAt = Instant.now()
    Set<BuyableToken> buyableTokens = []
    String creatorId = "creator-id"
    EventAddress address = new EventAddress("Poznan", "Hulewiczow", "60-688", null)

    static EventBuilder event() {
        new EventBuilder()
    }

    def build() {
        return new Event(
            id,
            type,
            name,
            description,
            startingAt,
            endingAt,
            buyableTokens,
            creatorId,
            address
        )
    }
}
