import org.springframework.beans.factory.annotation.Autowired
import pl.tokenevent.infra.payments.PayuAuthorizationService

class PayuIntegrationTest extends IntegrationSpec {

    @Autowired
    PayuAuthorizationService payuAuthorizationService

    def "should get payusandbox access token"() {
        when:
        def accessToken = payuAuthorizationService.accessToken

        then:
        accessToken != null
    }
}
