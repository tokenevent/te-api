import groovyx.net.http.HttpResponseException
import org.springframework.beans.factory.annotation.Autowired
import pl.tokenevent.domain.TokenStatus
import pl.tokenevent.domain.Transaction
import pl.tokenevent.domain.TransactionStatus
import pl.tokenevent.repositories.TransactionRepository

import java.time.Instant

import static groovyx.net.http.ContentType.JSON
import static pl.tokenevent.domain.TransactionStatus.REJECTED

//TODO split into transactions
class TransactionsSpec extends IntegrationSpec {

    @Autowired
    TransactionRepository transactionRepository

    def "should create pending transaction after scanning QR code"() {
        given:
        def tokens = buyTokensForUser()
        def buyableTokenId = tokens.first().buyableTokenId

        def walletResponse = restClient.get path: "/users/me/wallet",
            headers: getHeaders(validUserToken)
        walletResponse.status == 200
        def walletId = walletResponse.data.id

        def request = [
            walletId: walletId,
            tokens  : [[
                           id      : buyableTokenId,
                           quantity: 5
                       ]]
        ]

        when:

        def response = restClient.post path: "/scanner/transactions",
            body: request,
            headers: getHeaders(validAdminToken),
            requestContentType: JSON

        then:
        response.status == 200
        with(response.data.transaction) {
            id != null
            status == TransactionStatus.PENDING.toString()
            tokens.size() == 5
            tokens.each { it.status == TokenStatus.RESERVED }
        }
    }

    def "should respond with 404 when given transaction does not exist"() {
        when:
        getTransactionById("not-existing-id")

        then:
        def e = thrown(HttpResponseException)
        e.response.status == 409
        e.response.data.message == "Transaction with id: not-existing-id does not exist"
    }

    def "should respond with transaction when transaction exists"() {
        given:
        def transaction = createTransaction()

        when:
        def response = getTransactionById(transaction.id)

        then:
        response.status == 200
        with(response.data.transaction) {
            id == transaction.id
        }
    }

    def "should get my current pending transaction"() {
        given:
        String userId = getUserId(validUserToken)
        createTransaction("1", userId, TransactionStatus.ACCEPTED)
        createTransaction("2", userId, TransactionStatus.PENDING)
        createTransaction("3", userId, REJECTED)
        createTransaction("4", userId, REJECTED)

        when:
        def response = restClient.get path: "/my/transactions",
            headers: getHeaders(validUserToken)

        then:
        response.status == 200
        with(response.data.transaction) {
            id == "2"
        }
    }

    def "should accept/reject pending transaction"() {
        given:
        String userId = getUserId(validUserToken)
        createTransaction("1", userId, TransactionStatus.PENDING)

        when:
        def request = [
            transactionId: "1",
            decision     : decision
        ]
        def response = restClient.post path: "/my/transactions",
            body: request,
            headers: getHeaders(validUserToken),
            requestContentType: JSON

        then:
        response.status == 200
        with(response.data.transaction) {
            status == transactionStatus.toString()
        }

        where:
        decision | transactionStatus
        "REJECT" | REJECTED
        "ACCEPT" | TransactionStatus.ACCEPTED
    }

    def "should throw error when there is no pending transaction"() {
        when:
        restClient.post path: "/my/transactions",
            body: [
                transactionId: "1",
                decision     : "ACCEPT"
            ],
            headers: getHeaders(),
            requestContentType: JSON

        then:
        def e = thrown(HttpResponseException)
        e.response.status == 409
        e.response.data.message == "That user has no pending transaction"
    }

    def "should reject pending transaction"() {
        def tokens = buyTokensForUser()
        def buyableTokenId = tokens.first().buyableTokenId

        def walletResponse = restClient.get path: "/users/me/wallet",
            headers: getHeaders(validUserToken)
        walletResponse.status == 200
        def walletId = walletResponse.data.id

        def request = [
            walletId: walletId,
            tokens  : [[
                           id      : buyableTokenId,
                           quantity: 5
                       ]]
        ]

        when:
        def response = restClient.post path: "/scanner/transactions",
            body: request,
            headers: getHeaders(validAdminToken),
            requestContentType: JSON

        then:
        response.status == 200
        def transaction = response.data.transaction
        transaction.id != null
        transaction.status == TransactionStatus.PENDING.toString()
        transaction.tokens.size() == 5
        transaction.tokens.each { it.status == TokenStatus.RESERVED }

        when:
        restClient.delete path: "/scanner/transactions/${transaction.id}",
            headers: getHeaders(validAdminToken),
            requestContentType: JSON

        then:
        def rejectedTransaction = transactionRepository.findById(transaction.id as String)
        rejectedTransaction.get().status.toString() == REJECTED.toString()
    }

    def getTransactionById(String id) {
        restClient.get path: "/scanner/transactions/$id",
            headers: getHeaders(validAdminToken)
    }

    def createTransaction(String id = "some-id", String userId = "userId", TransactionStatus status = TransactionStatus.PENDING) {
        return transactionRepository.save(
            new Transaction(
                id,
                "scannerId",
                userId,
                "eventId",
                "eventName",
                [],
                status,
                Instant.now(),
                Instant.now()
            )
        )
    }
}
