import org.springframework.beans.factory.annotation.Autowired
import pl.tokenevent.domain.BuyTokenTransaction
import pl.tokenevent.repositories.BuyTokenTransactionsRepository

import static java.time.Instant.now
import static pl.tokenevent.domain.BuyTokenTransactionStatus.COMPLETED

class BuyTokenHistorySpecification extends IntegrationSpec {

    @Autowired
    BuyTokenTransactionsRepository buyTokenTransactionsRepository

    def "should return completed transaction update status"() {
        given:
        def userId = getUserId(validUserToken)
        buyTokenTransactionsRepository.save(new BuyTokenTransaction("trans-id", userId, [], "event-id", "event-name", now(), COMPLETED))

        when:
        def response = restClient.get path: "/my/buy-token-history/trans-id",
            headers: getHeaders(validUserToken)

        then:
        with(response.data) {
            it.currentStatus == "COMPLETED"
            it.tokenCount == 0
        }

    }
}
