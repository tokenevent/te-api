import groovyx.net.http.HttpResponseException
import org.springframework.beans.factory.annotation.Autowired
import pl.tokenevent.infra.users.UserRepository

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED

class SecurityIntegration extends IntegrationSpec {

    @Autowired
    UserRepository userRepository

    def "should pass somehow"() {
        given:
        def a = "a"
        when:
        def aa = a + "a"
        then:
        aa == "aa"
    }

    def "should successfully make request to public resource"() {
        given:
        def url = "/public/users/echo"

        when:
        def response = restClient.get path: url

        then:
        response.status == 200
        response.data.str == "echo"
    }

    def "should properly register login returning working token"() {
        given:
        def currentUserPath = "/users/current"
        def userCredentialsRequest = createUserCredentialsRequest("user_123", "password_!234")

        when:
        def user = registerUser(userCredentialsRequest)

        then:
        user.id != null
        user.username == userCredentialsRequest.username
        user.password == null
        user.createdAt != null

        when:
        def token = loginUser(userCredentialsRequest)
        def currentUserResponse = restClient.get path: currentUserPath,
            headers: ['Authorization': "Bearer ${token}"]

        then:
        currentUserResponse.status == 200
        currentUserResponse.data.enabled == true
        currentUserResponse.data.username == userCredentialsRequest.username
    }

    def "should create user with user and admin authorities"() {
        given:
        def registerPath = getPublicPath() + "/registeradmin"
        def request = createAdminCredentialsRequest("admin_test123")
        when:
        restClient.post path: registerPath,
            body: request,
            requestContentType: JSON

        then:
        def user = userRepository.findByUsername(request.username).get()
        user.authorities.find { it.authority == "ROLE_ADMIN" } != null
        user.authorities.find { it.authority == "ROLE_USER" } != null
    }

    def "should not log user with fail password and/or login"() {
        given:
        def loginPath = getPublicPath() + "/login"
        def request = createUserLoginRequest("dupa123", "dupa123")

        when:
        restClient.post path: loginPath,
            body: request,
            requestContentType: JSON

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == SC_UNAUTHORIZED
        e.response.data.status == "UNAUTHORIZED"
        e.response.data.message == "Bad login and/or password"

    }

    def "should properly serve rest api docs"() {
        given:
        def path = "/swagger-ui.html"

        when:
        def response = restClient.get path: path

        then:
        response.status == 200
        response.data.toString().startsWith("Swagger UI")
    }

    def "should be able to request scanner resource with scanner account"() {
        given:
        def event = createEvent()
        def login = "scanner"
        def password = "scanner_password"
        def request = createUserCredentialsRequest(login, password)

        def registerPath = "/events-manager/events/${event.id}/scanners"
        restClient.post path: registerPath,
            body: request,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        def loginPath = getPublicPath() + "/login"
        def response2 = restClient.post path: loginPath,
            body: createUserLoginRequest(login, password),
            requestContentType: JSON

        def validScannerToken = response2.data.str as String

        when:
        def response = restClient.get path: "/scanner/echo",
            headers: getHeaders(validScannerToken)

        then:
        response.status == 200
    }

    def "should not be able to request admin resource from scanner"() {
        given:
        def event = createEvent()
        def login = "scanner"
        def password = "scanner_password"
        def request = createUserCredentialsRequest(login, password)

        def registerPath = "/events-manager/events/${event.id}/scanners"
        restClient.post path: registerPath,
            body: request,
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        def loginPath = getPublicPath() + "/login"
        def response2 = restClient.post path: loginPath,
            body: createUserLoginRequest(login, password),
            requestContentType: JSON

        def validScannerToken = response2.data.str

        when:
        restClient.get path: "/admin/echo",
            headers: getHeaders(validScannerToken)

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == 403
    }
}
