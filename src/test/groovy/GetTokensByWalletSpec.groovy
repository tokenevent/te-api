class GetTokensByWalletSpec extends IntegrationSpec {

    def "should valid time should be decreasing"() {
        when:
        def firstWalletResponse = restClient.get path: "/users/me/wallet",
            headers: getHeaders(validUserToken)
        sleep(3000)
        def secondWalletResponse = restClient.get path: "/users/me/wallet",
            headers: getHeaders(validUserToken)

        then:
        firstWalletResponse.data.validForSeconds > secondWalletResponse.data.validForSeconds

    }
}
