import org.springframework.beans.factory.annotation.Autowired
import pl.tokenevent.infra.users.UserRepository
import pl.tokenevent.repositories.EventRepository

import static builders.EventBuilder.event
import static groovyx.net.http.ContentType.JSON
import static pl.tokenevent.infra.users.User.scannerUser

class EventManager extends IntegrationSpec {

    @Autowired
    EventRepository eventRepository

    @Autowired
    UserRepository userRepository

    def "should create scanner-user for event-admin"() {
        given:
        def userId = getUserId(validEventManagerToken)
        def event = eventRepository.save(event()
            .withId("1234")
            .withCreatorId(userId as String)
            .build()
        )

        when:
        restClient.post path: "/events-manager/events/1234/scanners",
            body: [
                username: "new-scanner",
                password: "newScannerPassword123"
            ],
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        def newUser = userRepository.findByUsername("new-scanner").get()
        newUser.parentId == userId
        newUser.eventId == event.id
    }

    def "scanner created by events-manager should log in"() {
        given:
        def userId = getUserId(validEventManagerToken)
        def event = eventRepository.save(event().withCreatorId(userId as String).build())

        def username = "new-scanner"
        def password = "newScannerPassword123"
        when:
        restClient.post path: "/events-manager/events/${event.id}/scanners",
            body: [
                username: username,
                password: password
            ],
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON
        def token = loginUser([
            username: username,
            password: password
        ])

        then:
        token != null
    }

    def "parent should be able to delete scanner"() {
        given:
        def userId = getUserId(validEventManagerToken)
        def scanner = userRepository.save(scannerUser("abc", "abc", "abc", userId))

        when:
        restClient.delete path: "/events-manager/events/abc/scanners/${scanner.id}",
            headers: getHeaders(validEventManagerToken),
            requestContentType: JSON

        then:
        !userRepository.findByUsername("abc").isPresent()
    }
}
